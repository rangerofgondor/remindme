<div align="center">
   <img src= "https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/launcher/logo.png" height=100> 

[![Made with Flutter][flutterdart]][flutter] [![Open Source Love][opensource]]() [![PR][PRs]]() [![Download][Download]][Download-Link] [![GPLv3 license][GPLv3]][License]

### An Android Application, made with Flutter :heart:, to maintain your schedule and be productive everyday!

</div>

# RemindMe

Plan your Today and Tomorrow with RemindMe, an easy to use reminder for your day-to-day tasks! Or want to be reminded at an arbitrary date? Just specify it and RemindMe will make sure you're notified!

# Screenshots

![Ss1][SS1]
![SS2][SS2]
![SS3][SS3]
![SS4][SS4]


# Features

 * Add a reminder by including title, date and time to be reminded with easy to access UI.
 * View all the reminders as a Timeline of events. 
 * View the reminders of today and tomorrow in dedicated tabs.
 * Quickly navigate to find the Upcoming, Overdue and Completed reminders.
 * Swipe right to complete a reminder, Swipe left to delete. It's that easy!
 * Long press to edit the reminder


# Contribution Guide

This project is open to any and all kinds of contribution in all of its categories.
For feature contribution, create a merge request with the feature as a banch and raise a Merge-Request.
If you have any bug report / suggestion for the project, feel free to open an issue and discuss.


#### Show some :heart: and :star: the repo to support the project!

If you found this project useful, then please consider giving it a :star: on Gitlab and sharing it with your friends via social media.


# Project Created & Maintained By

### Ranger Of Gondor ( Shrivathsa Prakash ✌ )

Freelancer, Flutter Developer, Android Developer, Gopher!

[![Linkedin][linkedin-badge]][linkedin] [![Portfolio Website][portfolio-badge]][portfolio]

### Maths Lover ( Maintainer and Contributer )

[![][mathslover-badge]][mathslover]

# Donate

If you found this project helpful or you learned something from the source code and want to thank me, consider buying me a cup of :coffee:

[![PayPal][PayPal-badge]][PayPal]

# Licence

```
Copyright @ 2020 Shrivathsa Prakash - GPL Version 3

                    GNU GENERAL PUBLIC LICENSE
                       Version 3, 29 June 2007

 Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

```
[flutter]: https://flutter.dev "Flutter"

[Logo]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/Logo.png
[SS1]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/screenshots/remindme-1.png
[SS2]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/screenshots/remindme-2.png
[SS3]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/screenshots/remindme-3.png
[SS4]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/screenshots/remindme-4.png
[Download-Link]: https://gitlab.com/rangerofgondor/remindme/-/raw/master/assets/apks/RemindMe.apk
[License]: https://lbesson.GPLv3-license.org

[portfolio]: https://rangerofgondor.gitlab.io/profile
[linkedin]: https://linkedin.com/in/shrivathsa-prakash
[PayPal]: https://www.paypal.me/rangerofgondor

[mathslover]: https://gitlab.com/maths_lover/

[flutterdart]: https://img.shields.io/badge/flutter-dart-blue?logo=flutter
[opensource]: https://badges.frapsoft.com/os/v1/open-source.png?v=103
[PRs]: https://img.shields.io/badge/Contributions-Welcome-pink?logo=gitlab
[Download]: https://img.shields.io/badge/Download-APK-green
[GPLv3]: https://img.shields.io/badge/License-GPL%20v3-orange.svg
[linkedin-badge]: https://img.shields.io/badge/Linked-In-blue?logo=linkedin
[portfolio-badge]: https://img.shields.io/badge/Portfolio-Website-blueviolet
[mathslover-badge]: https://img.shields.io/badge/maths-lover-orange?logo=gitlab
[PayPal-badge]: https://img.shields.io/badge/Pay-Pal-blue?logo=paypal
