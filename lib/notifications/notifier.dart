import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class NotificationManager{
  // ignore: non_constant_identifier_names
  final String CHANNEL_ID = "RemindMe";
  // ignore: non_constant_identifier_names
  final String CHANNEL_NAME = "RemindMeRemindMe.Notifications";
  // ignore: non_constant_identifier_names
  final String CHANNEL_DESC = "Send scheduled notifications for reminders";
  BuildContext context;
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin
  = FlutterLocalNotificationsPlugin();

  NotificationManager.init();

  static final NotificationManager _instance = NotificationManager.init();

  factory NotificationManager({BuildContext context}) {
    if(_instance==null)
      _instance.context = context;
    return _instance;
  }

  initialise() async {
    var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: _selectNotification);
  }

  Future _selectNotification(String payload) async{
    print('Payload : $payload');
    //Navigator.pushNamed(context, '/');
  }

  scheduleNotification({DateTime time, int id, String title,String desc,}) async{
    var scheduledNotificationDateTime = time;
    var androidPlatformChannelSpecifics =
    AndroidNotificationDetails(CHANNEL_ID,
        CHANNEL_NAME, CHANNEL_DESC,
        importance: Importance.High,
        priority: Priority.High,
        ticker: 'RemindMe'
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    NotificationDetails platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.schedule(
        id,
        title,
        desc,
        scheduledNotificationDateTime,
        platformChannelSpecifics
    );
  }

  Future cancelNotification(int id) async{
    await flutterLocalNotificationsPlugin.cancel(id);
  }

  Future cancelAllNotification(int id) async{
    await flutterLocalNotificationsPlugin.cancelAll();
  }
}