///These are deprecated widgets kept if at all they are needed in future
/*@deprecated
class _NormalCard extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    //TOTAL Height : 60 + 10  = 70
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10),
      child: Material(
        elevation: 0.0,
        child: Container(
          height: 60,
          child: InkWell(
            onTap: () {
              Provider.of<ExpansionManager>(context, listen: false).expanded =
              !Provider.of<ExpansionManager>(context, listen: false)
                  .expanded;
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Padding(
                    padding:
                    EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "Title",
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            TimeDisplay(time: '8:00 AM'),
                          ],
                        ),
                        Text(
                          "Description",
                          style: Theme.of(context).textTheme.caption,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

@deprecated
class _ExpandedCard extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    //TOTAL Height : 140 + 20  = 160
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 20.0),
      child: Card(
        margin: EdgeInsets.all(0),
        color: Colors.grey[400],
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            )),
        child: Container(
          height: 140.0,
          child: InkWell(
            onTap: () {
              Provider.of<ExpansionManager>(context, listen: false).expanded =
              !Provider.of<ExpansionManager>(context, listen: false)
                  .expanded;
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 16.0,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        "Title",
                        style: Theme.of(context).textTheme.bodyText1.copyWith(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      TimeDisplay(time: '8:00 AM'),
                    ],
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: Sizes.h(3)),
                    child: Text(
                      "Expanded\nDescription",
                      style: Theme.of(context).textTheme.caption,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

@deprecated
class _ReminderCardAccessories {
  Widget statusIndicatorLine(bool done) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(12.0, 0.0, 28.0, 0.0),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(4),
        child: Container(
          height: 30,
          width: 5,
          color: (!done) ? Colors.grey : Colors.black54,
        ),
      ),
    );
  }

  Widget checked(bool done) {
    return IconButton(
        icon: Icon((!done) ? Icons.check_box_outline_blank : Icons.check_box),
        color: (!done) ? Colors.grey : Colors.black54,
        onPressed: () {
          *//*setState(() {
            done = !done;
          });*//*
        });
  }
}

@deprecated
class CustomExpansionCard extends StatelessWidget {

  ///Custom Expanded Card needs to have a parent or ancestor of ExpansionManager Provider to work
  ///Usually used with "@FooTask" widget, Example : HomeTask or AllTask

  const CustomExpansionCard({
    Key key,
    @required this.titleRow,
    @required this.subtitleRow,
    @required this.expandedSubtitleRow,
    @required this.buttonRow,
  }) : super(key: key);
  final List<Widget> titleRow;
  final List<Widget> subtitleRow;

  final List<Widget> expandedSubtitleRow;
  final List<Widget> buttonRow;

  @override
  Widget build(BuildContext context) {

    bool isExpanded = Provider.of<ExpansionManager>(context).expanded;

    return AnimatedCrossFade(
      crossFadeState: (!isExpanded)
          ? CrossFadeState.showFirst
          : CrossFadeState.showSecond,
      duration: Duration(milliseconds: 200),
      firstCurve: Curves.easeInOut,
      secondCurve: Curves.easeInOut,
      firstChild: _commonChild(context, isExpanded),
      secondChild: _commonChild(context, isExpanded),
    );
  }

  Widget _commonChild(BuildContext context, bool isExpanded) {
    //TOTAL Height : 60 + 10  = 70
    //TOTAL Expanded Height : 140 + 20  = 160
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, (isExpanded) ? 20.0 : 10.0),
      child: Card(
        elevation: 0.0,
        margin: EdgeInsets.all(0),
        color: (isExpanded) ? Colors.grey[400] : null,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15.0),
            )),
        child: Container(
          height: (isExpanded) ? 140 : 60,
          child: InkWell(
            onTap: () {
              Provider.of<ExpansionManager>(context, listen: false).expanded =
              !isExpanded;
            },
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
              child: _expandedBuilder(isExpanded),
            ),
          ),
        ),
      ),
    );
  }

  Widget _expandedBuilder(bool isExpanded) {
    if (!isExpanded) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: titleRow),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: subtitleRow),
        ],
      );
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: titleRow),
          Expanded(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: expandedSubtitleRow),
          ),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: buttonRow),
        ],
      );
    }
  }
}

@deprecated
class ExpansionManager with ChangeNotifier {
  bool _expanded = false;

  ExpansionManager({bool initial = false}) {
    _expanded = initial;
  }
  bool get expanded => _expanded;

  set expanded(bool expanded) {
    this._expanded = expanded;
    notifyListeners();
  }
}*/

///Deprecated when trying to make UI update only when Today or Tomorrow
///was being built

/*
class EventHomeToday extends EventHome {
  const EventHomeToday();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventHomeToday';
}

class EventHomeTomorrow extends EventHome {
  const EventHomeTomorrow();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventHomeTomorrow';
}*/
///Bloc States
/*
abstract class StateHome extends ReminderState {
  final List<Reminder> reminders;

  const StateHome({
    this.reminders,
  });

  StateHome copyWith({List<Reminder> reminders,});

  @override
  List<Object> get props => [reminders];

  @override
  String toString() =>
      'StateHome {Reminders: ${reminders.length},';
}

class StateHomeToday extends StateHome {
  final List<Reminder> reminders;

  const StateHomeToday({
    this.reminders,
  });

  StateHomeToday copyWith({
    List<Reminder> reminders,
  }) {
    return StateHomeToday(
      reminders: reminders ?? this.reminders,
    );
  }

  @override
  List<Object> get props => [reminders];

  @override
  String toString() =>
      'StateHomeToday {Reminders: ${reminders.length},';
}

class StateHomeTomorrow extends StateHome {
  final List<Reminder> reminders;

  const StateHomeTomorrow({
    this.reminders,
  });

  StateHomeTomorrow copyWith({
    List<Reminder> reminders,
  }) {
    return StateHomeTomorrow(
      reminders: reminders ?? this.reminders,
    );
  }

  @override
  List<Object> get props => [reminders];

  @override
  String toString() =>
      'StateHomeTomorrow {Reminders: ${reminders.length},';
}
 */
///Home Page TabView with children as BlocBuilders

/*
TabBarView(
            controller: _tabController,
            children: [
              BlocBuilder<ReminderBloc, ReminderState>(
                builder: (context, state) {
                  if(state is StateHomeToday) {
                    final todayReminders = state.reminders;
                    print('$todayReminders $todayReminders');
                    return (todayReminders.isNotEmpty)
                        ? CustomList(reminders: todayReminders,)
                        : Center(
                        child: Text("No Reminders for Today ;-)")
                    );
                  }
                  else{
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
              BlocBuilder<ReminderBloc, ReminderState>(
                builder: (context, state) {
                  if(state is StateHomeTomorrow) {
                    final tomorrowReminders = state.reminders;
                    print('$tomorrowReminders $tomorrowReminders');
                    return (tomorrowReminders.isNotEmpty)
                        ? CustomList(reminders: tomorrowReminders,)
                        : Center(
                        child: Text("No Reminders for Tomorrow ;-)")
                    );
                  }
                  else{
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ],
          ),
 */