import 'package:flutter/material.dart';

class Sizes {
  static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double bWidth;
  static double bHeight;
  static double pixRatio;


  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    pixRatio = _mediaQueryData.devicePixelRatio;
    bWidth = screenWidth / 100;
    bHeight = screenHeight / 100;
  }

  static double h(double size){
    assert (size <= 100 && size >= 0);
      return bHeight * size;
  }

  static double w(double size){
    assert (size <= 100 && size >= 0);
    return bWidth * size;
  }
}
enum SOrientation{
  width,
  height
}