import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/expansion_bloc/events.dart';
import 'package:remindme/models/expansion_bloc/expansion_bloc.dart';
import 'package:remindme/models/expansion_bloc/states.dart';
import 'package:remindme/models/model.dart';
import 'package:remindme/pages/add_edit/editReminder.dart';
import 'package:remindme/pages/common/widgets.dart';
import 'package:remindme/pages/default/home_default.dart';

class CustomList extends StatelessWidget {
  final List<Reminder> reminders;
  final bool isToday;

  const CustomList({
    Key key,
    this.reminders,
    this.isToday = false,
  }) : super(key:key);

  @override
  Widget build(BuildContext context) {
    List<Reminder> inComplete = [];
    List<Reminder> completedReminders = [];
    reminders.forEach((reminder) {
      if(reminder.isCompleted)
        completedReminders.add(reminder);
      else
        inComplete.add(reminder);
    });
    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: RefreshIndicator(
        onRefresh: () {
          BlocProvider.of<ReminderBloc>(context).add(EventLoading());
          return Future.delayed(Duration(milliseconds: 500));
        },
        child: ListView(
          children: [
            CompletedSection(completedReminders: completedReminders,),
            (inComplete.length>0)
                ? BlocProvider(
              create: (context) => ExpansionBloc(),
              child: Column(
              children: List<Widget>.generate(inComplete.length, (index) {
                if (index == 0) {
                  return HomeTask(
                    reminder: inComplete[index],
                    index: index,
                    last: (inComplete.length == 1) ? true : false,
                  );
                }
                else if(index == inComplete.length-1){
                  return HomeTask(
                    reminder: inComplete[index],
                    index: index,
                    last: true,
                  );
                }
                return HomeTask(
                  reminder: inComplete[index],
                  index: index,
                );
              }),
              ),
            )
                :HomeCompletedDefault(
              isToday: isToday,
            ),
          ],
        ),
      ),
    );
  }
}

class HomeTask extends StatelessWidget {
  HomeTask({
    Key key,
    @required this.reminder,
    @required this.index,
    this.last = false,
  }) : super(key: key);

  final Reminder reminder;
  final bool last;
  final int index;
  final Color yellowTheme = Color(0xFFFABB18);

  @override
  Widget build(BuildContext context) {
    if(index ==0)BlocProvider.of<ExpansionBloc>(context).add(EEventExpand(index: index));
    return BlocBuilder<ExpansionBloc,ExpansionState>(
      condition: (previous, current) {
        if(current.index == index){
          return true;
        }
        return false;
      },
      builder: (_, state) {
        bool isExpanded = false;
        if(state is EStateExpand && state.index == index) isExpanded = true;
        else if(state is EStateCollapse && state.index == index) isExpanded = false;
        return AnimatedCrossFade(
          crossFadeState: (isExpanded)
              ?CrossFadeState.showFirst
              :CrossFadeState.showSecond,
          firstChild: _buildChild(isExpanded, context),
          secondChild: _buildChild(isExpanded, context),
          firstCurve: Curves.easeInOut,
          secondCurve: Curves.easeInOut,
          duration: Duration(milliseconds: 200),
        );
      },
    );
  }

  Widget _buildChild(bool isExpanded, context) {
    var timeFormat = DateFormat.jm();
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Spacer(
          flex: 1,
        ),
        Separator(last: last, expanded: isExpanded,),
        Spacer(
          flex: 1,
        ),
        Expanded(
          flex: 15,
          child: CustomExpansionCard(
            isExpanded: isExpanded,
            cardKey: reminder.id.toString(),
            onPressed: (){
              if (!isExpanded)
                BlocProvider.of<ExpansionBloc>(context).add(EEventPressed(index: index));
              else
                BlocProvider.of<ExpansionBloc>(context).add(EEventCollapse(index: index));
            },
            onLongPressed: (){
              CustomBottomSheet.of(
                context: context,
                title: 'Edit Reminder',
                child: EditReminderPage(
                  reminder: reminder,
                  onSubmitted: ({reminder}) {
                    BlocProvider.of<ReminderBloc>(context).add(EventEdit(reminder: reminder));
                  },
                ),
              ).show();
            },
            onDeleted: (){
              BlocProvider.of<ReminderBloc>(context).add(EventDelete(id: reminder.id));
            },
            onCompleted: (){
              BlocProvider.of<ReminderBloc>(context).add(EventCompleted(id: reminder.id));
            },
            titleRow: [
              Text(
                reminder.title,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 18.0,
                    color: Colors.black
                ),
              ),
              RawChip(
                backgroundColor: Colors.white,
                elevation: 0.0,
                padding: EdgeInsets.all(0.0),
                label: TimeDisplay(time: timeFormat.format(reminder.dateTime),),
              ),
            ],
            expandedSubtitleRow: (isExpanded)?[
              (reminder.notes.isNotEmpty)?Text(
                reminder.notes,
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14.0,
                    color: Colors.black
                ),
              )
                  :ActionChip(
                backgroundColor: Colors.white,
                onPressed: (){
                  CustomBottomSheet.of(
                    context: context,
                    title: 'Add a Note',
                    child: EditReminderPage(
                      reminder: reminder,
                      onlyNotes: true,
                      onSubmitted: ({reminder}) {
                        BlocProvider.of<ReminderBloc>(context).add(EventEdit(reminder: reminder));
                      },
                    ),
                  ).show();
                },
                label: Row(
                  children: [
                    Text(
                        'Add a note  ',
                      textAlign: TextAlign.start,
                    ),
                    Icon(Icons.add_circle, color: Colors.black,)
                  ],
                ),
                labelStyle: TextStyle(
                    fontWeight: FontWeight.w300,
                    fontSize: 14.0,
                    color: Colors.black
                ),
              ),
            ]:null,
          ),
        ),
        Spacer(
          flex: 1,
        ),
      ],
    );
  }
}

class CompletedSection extends StatelessWidget {
  const CompletedSection({Key key, this.completedReminders}):super(key: key);

  final List<Reminder> completedReminders;
  @override
  Widget build(BuildContext context) {
    return (completedReminders.length > 0)
        ?Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: ExpansionTile(
            leading: Icon(Icons.done),
      trailing: Icon(Icons.add),
      title: Text('Completed ( ${completedReminders.length} )'),
      children: List.generate(completedReminders.length,
              (index) => CompletedCard(
            reminder: completedReminders[index],
            onDismissed: (){
              BlocProvider.of<ReminderBloc>(context).add(
                  EventDelete(
                      id: completedReminders[index].id
                  )
              );
            },
            onLongPressed: (){
              CustomBottomSheet.of(
                context: context,
                title: 'Edit Reminder',
                child: EditReminderPage(
                  reminder: completedReminders[index],
                  onSubmitted: ({reminder}) {
                    BlocProvider.of<ReminderBloc>(context).add(
                        EventEdit(
                            reminder: reminder
                        )
                    );
                  },
                ),
              ).show();
            },
          ),
      ),
    ),
        )
        :Container();
  }
}


class CompletedCard extends StatelessWidget {
  const CompletedCard({
    Key key,
    @required this.reminder,
    @required this.onLongPressed,
    @required this.onDismissed
  }):super(key: key);

  final Reminder reminder;
  final Function onLongPressed;
  final Function onDismissed;
  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: Key(reminder.id.toString()),
      onDismissed: (direction) {
        onDismissed();
      },
      background: Slide(isLeft: true,),
      secondaryBackground: Slide(),
      child: Container(
        height: 60,
        child: InkWell(
          splashColor: Color(0x33000000),
          focusColor: Colors.white,
          highlightColor: Color(0x55FFFFFF),
          onLongPress: () {
            onLongPressed();
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  reminder.title,
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18.0,
                      color: Colors.black
                  ),
                ),
                RawChip(
                  backgroundColor: Colors.white,
                  elevation: 0.0,
                  padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(16.0)),
                      side: BorderSide(
                          width: 1.0,
                          color: Colors.black
                      )
                  ),
                  label: Row(
                    children: [
                      TimeDisplay(time: DateFormat.jm().format(reminder.dateTime),)
                    ],
                  ),
                  labelStyle: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: 18.0,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}


class Separator extends StatelessWidget {
  Separator({
    this.last = false,
    this.expanded = false
  });

  final Color yellowTheme = Color(0xFFFABB18);
  final bool last, expanded;
  @override
  Widget build(BuildContext context) {
    //TOTAL Height : 10 + 15 + 10 + 35 = 70
    //TOTAL Expanded Height : 10 + 15 + 10 + 125 = 160
    return Column(children: [
      SizedBox(
        height: 10.0,
      ),
      _circle(expanded),
      SizedBox(
        height: 10.0,
      ),
      (!last)
          ?(expanded)
          ?Container(
        height: 125,
        width: 2,
        color: yellowTheme,)
          :Container(
        height: 35,
        width: 2,
        color: yellowTheme,
      )
          : SizedBox(
        height: 0,
        width: 0,
      ),
    ]);
  }

  Widget _circle(bool expanded) {
    return Container(
      height: 15.0,
      width: 15.0,
      child: CustomPaint(
        painter:
            (expanded) ? ExpandedCircleCustomPainter() : CircleCustomPainter(),
      ),
    );
  }
}

class CircleCustomPainter extends CustomPainter {
  final Color yellowTheme = Color(0xFFFABB18);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = yellowTheme
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    canvas.drawCircle(size.center(Offset.zero), size.height * 0.4, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class ExpandedCircleCustomPainter extends CustomPainter {
  final Color yellowTheme = Color(0xFFFABB18);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = yellowTheme
      ..style = PaintingStyle.stroke
      ..strokeWidth = 1.0;

    canvas.drawCircle(size.center(Offset.zero), size.height * 0.7, paint);

    paint = Paint()..color = yellowTheme;
    canvas.drawCircle(size.center(Offset.zero), size.height * 0.4, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}