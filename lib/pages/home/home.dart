import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/bloc/state.dart';
import 'package:remindme/models/model.dart';
import 'package:remindme/pages/common/widgets.dart';
import 'package:remindme/pages/default/home_default.dart';
import 'package:remindme/pages/home/widgets.dart';
import 'package:remindme/screen_info/sizes.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Sizes().init(context);
    return SafeArea(
      maintainBottomViewPadding: true,
      child: Scaffold(
        backgroundColor: Colors.white,
        primary: true,
        body: RefreshIndicator(
          onRefresh: () {
            BlocProvider.of<ReminderBloc>(context).add(EventLoading());
            return Future.delayed(Duration(milliseconds: 500));
          },
          child: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return [
                CustomAppBar(
                  controller: tabController,
                ),
                SliverPersistentHeader(
                  pinned: true,
                  delegate: TabBarSliver(
                    minHeight: 55,
                    maxHeight: 58,
                    tabController: tabController,
                    tabs: ['Today', 'Tomorrow'],
                  ),
                ),
              ];
            },
            body: BlocConsumer<ReminderBloc, ReminderState>(
              buildWhen: (previous, current) =>
                  (current is StateHome || current is StateLoading),
              builder: (context, state) {
                if (state is StateHome) {
                  final todayReminders = state.todayReminders;
                  final tomorrowReminders = state.tomorrowReminders;
                  print('Home List : $todayReminders $tomorrowReminders');
                  return TabBarView(
                    controller: tabController,
                    children: [
                      (todayReminders.isNotEmpty)
                          ? CustomList(
                        isToday: true,
                        reminders: todayReminders,
                            )
                          : TodayDefault(),
                      (tomorrowReminders.isNotEmpty)
                          ? CustomList(
                        reminders: tomorrowReminders,
                            )
                          : TomorrowDefault(),
                    ],
                  );
                } else {
                  BlocProvider.of<ReminderBloc>(context).add(EventHome());
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
              listenWhen: (previous, current) {
                if (current is StateFineFocus) return true;
                return false;
              },
              listener: (context, state) {
                tabController
                    .animateTo((state as StateFineFocus).tabIndex.index);
              },
            ),
          ),
        ),
      ),
    );
  }
}

class CustomAppBar extends StatefulWidget {
  CustomAppBar({
    Key key,
    @required this.controller,
  }) : super(key: key);

  final TabController controller;

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  final Color yellowTheme = Color(0xFFFABB18);
  String todayDay, todayDate, todayMonth;
  String tomorrowDay, tomorrowDate, tomorrowMonth;
  homeTabs current;

  @override
  void initState() {
    super.initState();
    current = homeTabs.today;
    widget.controller.addListener(() {
      if (widget.controller.index == 0)
        current = homeTabs.today;
      else
        current = homeTabs.tomorrow;
      setState(() {});
    });
    var today = DateTime.now();
    var tomorrow = today.add(Duration(days: 1));
    var dayFormat = DateFormat.E();
    var dateFormat = DateFormat.d();
    var monthFormat = DateFormat.MMM();

    todayDay = dayFormat.format(today);
    todayDate = dateFormat.format(today);
    todayMonth = monthFormat.format(today);

    tomorrowDay = dayFormat.format(tomorrow);
    tomorrowDate = dateFormat.format(tomorrow);
    tomorrowMonth = monthFormat.format(tomorrow);
  }

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      backgroundColor: Colors.white,
      elevation: 0.0,
      title: Padding(
        padding: const EdgeInsets.only(top: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "Remind",
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(fontWeight: FontWeight.w200, color: Colors.black),
            ),
            Text(
              "Me",
              style: Theme.of(context)
                  .textTheme
                  .headline4
                  .copyWith(fontWeight: FontWeight.w900, color: yellowTheme),
            ),
          ],
        ),
      ),
      pinned: true,
      expandedHeight: 170,
      flexibleSpace: FlexibleSpaceBar(
        collapseMode: CollapseMode.parallax,
        centerTitle: true,
        background: Padding(
          padding: const EdgeInsets.only(left: 16, right: 16, top: 60),
          child: BlocBuilder<ReminderBloc, ReminderState>(
              condition: (previous, current) => (current is StateHome),
              builder: (context, state) {
                if (state is StateHome) {
                  final List<Reminder> today =
                      state.todayReminders;
                  final List<Reminder> tomorrow =
                      state.tomorrowReminders;
                  int todayCount = 0, tomorrowCount = 0;
                  today.forEach((reminder) {
                    if(!reminder.isCompleted)
                      todayCount++;
                  });
                  tomorrow.forEach((reminder) {
                    if(!reminder.isCompleted)
                      tomorrowCount++;
                  });
                  return AnimatedSwitcher(
                      duration: Duration(milliseconds: 300),
                      transitionBuilder: (child, animation) {
                        final  offsetAnimation =
                        Tween<Offset>(begin: Offset(-1.0, 0.0), end: Offset(0.0, 0.0)).animate(animation);
                        return SlideTransition(
                          position: offsetAnimation,
                          child: child,
                        );
                      },
                      child: Container(
                        key: Key(current.index.toString()),
                        child: _animatedBuilder(current, (current == homeTabs.today)
                            ?todayCount
                            :tomorrowCount
                        ),
                      )
                  );
                } else
                  return Container();
              }),
        ),
        stretchModes: [StretchMode.fadeTitle],
      ),
    );
  }

  Widget _animatedBuilder(homeTabs current, int count){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              (current == homeTabs.today)
                  ? "$todayDay, "
                  : "$tomorrowDay, ",
              style:
              Theme.of(context).textTheme.overline.copyWith(
                fontWeight: FontWeight.w300,
                color: Colors.black,
                fontSize: 18,
              ),
            ),
            Text(
              (current == homeTabs.today)
                  ? "$todayDate "
                  : "$tomorrowDate ",
              style:
              Theme.of(context).textTheme.overline.copyWith(
                fontWeight: FontWeight.w600,
                color: yellowTheme,
                fontSize: 18,
              ),
            ),
            Text(
              (current == homeTabs.today)
                  ? "$todayMonth"
                  : "$tomorrowMonth",
              style:
              Theme.of(context).textTheme.overline.copyWith(
                fontWeight: FontWeight.w300,
                color: Colors.black,
                fontSize: 18,
              ),
            ),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "You have ",
              style:
              Theme.of(context).textTheme.headline6.copyWith(
                fontWeight: FontWeight.w100,
                color: Colors.black,
              ),
            ),
            Text((count > 0)
              ?" $count "
              :" NO ",
              style:
              Theme.of(context).textTheme.headline6.copyWith(
                fontWeight: FontWeight.w900,
                color: yellowTheme,
              ),
            ),
            Text((count == 1)
                  ? " task for "
                  : " tasks for ",
              style:
              Theme.of(context).textTheme.headline6.copyWith(
                fontWeight: FontWeight.w100,
                color: Colors.black,
              ),
            ),
            Text(
              (current == homeTabs.today)
                  ? "today"
                  : "tomorrow",
              style:
              Theme.of(context).textTheme.headline6.copyWith(
                fontWeight: FontWeight.w100,
                color: Colors.black,
              ),
            ),
          ],
        )
      ],
    );
  }
}

enum homeTabs { today, tomorrow }
