import 'package:flutter/material.dart';
final Color yellowTheme = Color(0xFFFABB18);

class UpcomingDefault extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'images/upcoming.jpg',
          height: 180.0,
          width: 180.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 20.0),
        Text(
          "Stay calm! ",
          style: TextStyle(
              fontWeight: FontWeight.w900,
              color: Colors.black,
              fontSize: 34.0),
        ),
        SizedBox(height: 20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "You have no ",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
            Text(
              "upcoming ",
              style: TextStyle(
                  fontWeight: FontWeight.w800, color: yellowTheme, fontSize: 18.0),
            ),
            Text(
              "tasks. ",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
          ],
        ),
        SizedBox(height: 20.0),
      ],
    );
  }
}

class OverdueDefault extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'images/overdue.jpg',
          height: 180.0,
          width: 180.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 20.0),
        Text(
          "Keep it up!",
          style: TextStyle(
              fontWeight: FontWeight.w900,
              color: Colors.black,
              fontSize: 34.0),
        ),
        SizedBox(height: 20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "You have no ",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
            Text(
              "overdue ",
              style: TextStyle(
                  fontWeight: FontWeight.w800, color: yellowTheme, fontSize: 18.0),
            ),
            Text(
              "tasks. ",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
          ],
        ),
        SizedBox(height: 20.0),
      ],
    );
  }
}

class CompletedDefault extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'images/completed.jpg',
          height: 180.0,
          width: 180.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 20.0),
        Text(
          "Oh, no!",
          style: TextStyle(
              fontWeight: FontWeight.w900,
              color: Colors.black,
              fontSize: 34.0),
        ),
        SizedBox(height: 20.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "You have no ",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
            Text(
              "completed ",
              style: TextStyle(
                  fontWeight: FontWeight.w800, color: yellowTheme, fontSize: 18.0),
            ),
            Text(
              "tasks.",
              style: TextStyle(
                  fontWeight: FontWeight.w200, color: Colors.black, fontSize: 18.0),
            ),
          ],
        ),
        SizedBox(height: 20.0),
      ],
    );
  }
}