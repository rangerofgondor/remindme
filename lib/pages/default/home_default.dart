import 'package:flutter/material.dart';
final Color yellowTheme = Color(0xFFFABB18);

class TodayDefault extends StatelessWidget {
  const TodayDefault({
    Key key,
  }):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Spacer(flex: 4,),
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "A ",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                    fontSize: 28.0),
              ),
              Text(
                "fresh ",
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    color: yellowTheme,
                    fontSize: 28.0),
              ),
              Text(
                "start!",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                    fontSize: 28.0),
              ),
            ]),
        Image.asset(
          'images/sunrise.png',
          height: 150.0,
          width: 150.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        Text(
          "Do you want to add a new task?",
          style: TextStyle(
              fontWeight: FontWeight.w200, color: Colors.black, fontSize: 16.0),
        ),
        Spacer(flex: 3,),
        Image.asset(
          'images/arrow.jpg',
          alignment: Alignment.center,
          height: 60.0,
          width: 60.0,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 20,),
      ],
    );
  }
}

class TomorrowDefault extends StatelessWidget {
  const TomorrowDefault({
    Key key,
  }):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Spacer(flex: 4,),
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Plan your ",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                    fontSize: 28.0),
              ),
              Text(
                "tomorrow ",
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    color: yellowTheme,
                    fontSize: 28.0),
              ),
            ]),
        Image.asset(
          'images/sunrise-tomorrow.png',
          height: 150.0,
          width: 150.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        Text(
          "Start by adding a new task",
          style: TextStyle(
              fontWeight: FontWeight.w200, color: Colors.black, fontSize: 16.0),
        ),
        Spacer(flex: 3,),
        Image.asset(
          'images/arrow.jpg',
          alignment: Alignment.center,
          height: 60.0,
          width: 60.0,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 20,),
      ],
    );
  }
}

class HomeCompletedDefault extends StatelessWidget {
  const HomeCompletedDefault({
    Key key,
    this.isToday = false,
  }):super(key: key);

  final bool isToday;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(height: 40,),
        Image(image: AssetImage('images/tick.png'),
          //'images/tick.png',
          height: 80.0,
          width: 80.0,
          alignment: Alignment.center,
          fit: BoxFit.fill,
        ),
        SizedBox(height: 5,),
        Row(
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "Nicely ",
                style: TextStyle(
                    fontWeight: FontWeight.w900,
                    color: yellowTheme,
                    fontSize: 28.0),
              ),
              Text(
                "done!",
                style: TextStyle(
                    fontWeight: FontWeight.w300,
                    color: Colors.black,
                    fontSize: 28.0),
              ),
            ]),
        SizedBox(height: 20,),
        Text((isToday)
            ?"You've completed all your tasks for today."
            :'Looks like you are working overtime!',
          style: TextStyle(
              fontWeight: FontWeight.w200, color: Colors.black, fontSize: 14.0),
        ),
      ],
    );
  }
}
