import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/expansion_bloc/events.dart';
import 'package:remindme/models/expansion_bloc/expansion_bloc.dart';
import 'package:remindme/models/expansion_bloc/states.dart';
import 'package:remindme/models/model.dart';
import 'package:remindme/pages/add_edit/editReminder.dart';
import 'package:remindme/pages/common/widgets.dart';

class AllReminderList extends StatelessWidget {
  final List<GroupedReminders> groups;

  const AllReminderList({Key key, this.groups}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    int sequentialIndex = 0;
    return BlocProvider(
        create: (context) => ExpansionBloc(),
        child: RefreshIndicator(
          onRefresh: () {
            BlocProvider.of<ReminderBloc>(context).add(EventLoading());
            return Future.delayed(Duration(milliseconds: 500));
          },
          child: ListView(
              children: List<Widget>.generate(groups.length, (index) {
            var group = groups[index];
            return ExpansionTile(
              backgroundColor: Colors.white,
              title: Text(group.groupTitle,
                  style: Theme.of(context).textTheme.headline6),
              trailing: Icon(Icons.add),
              initiallyExpanded: index==0 ?true:false,
              children: List<Widget>.generate(group.reminders.length, (index) {
                if (index == 0) {
                  print(sequentialIndex);
                  if (sequentialIndex == 0)
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 8.0, horizontal: 16.0),
                        child: Container(
                          height: 1.0,
                          width: MediaQuery.of(context).size.width * 0.7,
                          color: Colors.grey[300],
                        ),
                      ),
                      AllTask(
                        reminder: group.reminders[index],
                        index: sequentialIndex++,
                      ),
                    ],
                  );
                }
                return AllTask(
                  reminder: group.reminders[index],
                  index: sequentialIndex++,
                );
              }),
            );
          })),
        ));
  }
}

class AllTask extends StatelessWidget {
  AllTask({
    Key key,
    @required this.index,
    @required this.reminder,
  }) : super(key: key);

  final int index;
  final Reminder reminder;

  @override
  Widget build(BuildContext context) {
    if(index ==0)BlocProvider.of<ExpansionBloc>(context).add(EEventExpand(index: index));
    return BlocBuilder<ExpansionBloc, ExpansionState>(
      condition: (previous, current) {
        if (current.index == index) {
          return true;
        }
        return false;
      },
      builder: (context, state) {
        bool isExpanded = false;
        if (state is EStateExpand && state.index == index)
          isExpanded = true;
        else if (state is EStateCollapse && state.index == index)
          isExpanded = false;
        return AnimatedCrossFade(
          crossFadeState: (isExpanded)
              ? CrossFadeState.showFirst
              : CrossFadeState.showSecond,
          firstChild: _buildChild(isExpanded, context),
          secondChild: _buildChild(isExpanded, context),
          firstCurve: Curves.easeInOut,
          secondCurve: Curves.easeInOut,
          duration: Duration(milliseconds: 200),
        );
      },
    );
  }

  Widget _buildChild(bool isExpanded, BuildContext context) {
    var dateFormat = DateFormat.d();
    var dayFormat = DateFormat.E();
    var fullDayFormat = DateFormat.EEEE();
    var timeFormat = DateFormat.jm();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 2.0),
      child: CustomExpansionCard(
        isExpanded: isExpanded,
        cardKey: reminder.id.toString(),
        onPressed: () {
          if (!isExpanded)
            BlocProvider.of<ExpansionBloc>(context).add(EEventPressed(index: index));
          else
            BlocProvider.of<ExpansionBloc>(context).add(EEventCollapse(index: index));
        },
        onLongPressed: (){
          CustomBottomSheet.of(
            context: context,
            title: 'Edit Reminder',
            child: EditReminderPage(
              reminder: reminder,
              onSubmitted: ({reminder}) {
                BlocProvider.of<ReminderBloc>(context).add(EventEdit(reminder: reminder));
              },
            ),
          ).show();
        },
          onDeleted: (){
            BlocProvider.of<ReminderBloc>(context)
                .add(EventDelete(id: reminder.id));
          },
          onCompleted: (!reminder.isCompleted)?(){
            BlocProvider.of<ReminderBloc>(context)
                .add(EventCompleted(id: reminder.id));
          }:null,
        titleRow: [
          Text(
            reminder.title,
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 18.0,
                color: Colors.black
            ),
          ),
          RawChip(
            backgroundColor: Colors.white,
            elevation: 0.0,
            padding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 0.0),
            shape: (!isExpanded)?RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(16.0)),
              side: BorderSide(
                width: 1.0,
                color: Colors.black
              )
            ):null,
            label: Row(
              children: [
                (!isExpanded)?Text(
                  dayFormat.format(reminder.dateTime).toUpperCase(),
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: 14.0,
                      color: Colors.black
                  ),
                  textAlign: TextAlign.center,
                ):Container(),
                (!isExpanded)?SizedBox(width: 15,):Container(),
                Text(dateFormat.format(reminder.dateTime),
                ),
              ],
            ),
            labelStyle: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 18.0,
                color: Colors.black
            ),
          ),
        ],
        expandedSubtitleRow: (isExpanded)?[
          (reminder.notes.isNotEmpty)?Text(
            reminder.notes,
            style: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 14.0,
                color: Colors.black
            ),
          )
              :ActionChip(
            backgroundColor: Colors.white,
            onPressed: (){
              CustomBottomSheet.of(
                context: context,
                title: 'Add a Note',
                child: EditReminderPage(
                  reminder: reminder,
                  onlyNotes: true,
                  onSubmitted: ({reminder}) {
                    BlocProvider.of<ReminderBloc>(context).add(EventEdit(reminder: reminder));
                  },
                ),
              ).show();
            },
            label: Row(
              children: [
                Text(
                  'Add a note  ',
                  textAlign: TextAlign.start,
                ),
                Icon(Icons.add_circle, color: Colors.black,)
              ],
            ),
            labelStyle: TextStyle(
                fontWeight: FontWeight.w300,
                fontSize: 14.0,
                color: Colors.black
            ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                fullDayFormat.format(reminder.dateTime),
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 16.0,
                    color: Colors.black
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 16.0,),
              TimeDisplay(
                time: timeFormat.format(reminder.dateTime),
              )
            ],
          ),
        ]:null
      ),
    );
  }
}
