import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/bloc/state.dart';
import 'package:remindme/pages/all/widgets.dart';
import 'package:remindme/pages/common/widgets.dart';
import 'package:remindme/pages/default/all_default.dart';
import 'package:remindme/screen_info/sizes.dart';

class AllPage extends StatefulWidget {
  @override
  _AllPageState createState() => _AllPageState();
}

class _AllPageState extends State<AllPage>
    with SingleTickerProviderStateMixin {
  TabController tabController;
  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Sizes().init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () {
          BlocProvider.of<ReminderBloc>(context).add(EventLoading());
          return Future.delayed(Duration(milliseconds: 500));
        },
        child: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return [
              SliverAppBar(
                elevation: 0.0,
                backgroundColor: Colors.white,
                title: Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Remind",
                        style: Theme.of(context).textTheme.headline4.copyWith(
                            fontWeight: FontWeight.w200,
                            color: Colors.black
                        ),
                      ),
                      Text("Me",
                        style: Theme.of(context).textTheme.headline4.copyWith(
                            fontWeight: FontWeight.w900,
                            color: yellowTheme
                        ),
                      ),
                    ],
                  ),
                ),
                centerTitle: true,
                pinned: true,
              ),
              SliverPersistentHeader(
                pinned: true,
                delegate: TabBarSliver(
                    minHeight: 55,
                    maxHeight: 58,
                    tabController: tabController,
                  tabs: [
                    'Upcoming',
                    'Overdue',
                    'Completed'
                  ],
                ),
              ),
            ];
          },
          body: BlocConsumer<ReminderBloc, ReminderState>(
            buildWhen: (previous, current)
            => (current is StateAll || current is StateLoading),
            builder: (context, state) {
              if(state is StateAll) {
                final upcomingReminders = state.upcomingReminders;
                final overdueReminders = state.overdueReminders;
                final completedReminders = state.completedReminders;

                return TabBarView(
                  controller: tabController,
                  children: [
                    (upcomingReminders.isNotEmpty)
                        ? AllReminderList(groups: upcomingReminders,)
                        : UpcomingDefault(),
                    (overdueReminders.isNotEmpty)
                        ? AllReminderList(groups: overdueReminders,)
                        : OverdueDefault(),
                    (completedReminders.isNotEmpty)
                        ? AllReminderList(groups: completedReminders,)
                        : CompletedDefault(),
                  ],
                );
              }
              else{
                BlocProvider.of<ReminderBloc>(context).add(EventAll());
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
            listenWhen: (previous, current) {
              if(current is StateFineFocus)
                return true;
              return false;
            },
            listener: (context, state) {
              print('Executed all listener');
              switch(((state as StateFineFocus).tabIndex)){
                case focus.today:
                case focus.tomorrow:
                case focus.upcoming:
                  tabController.animateTo(0);
                  break;
                case focus.overdue:
                  tabController.animateTo(1);
                  break;
              }
            },
          ),
        ),
      ),
    );
  }
}
