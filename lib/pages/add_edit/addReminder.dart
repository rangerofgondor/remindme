import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:remindme/models/model.dart';
import 'package:remindme/pages/common/widgets.dart';


class AddReminderPage extends StatefulWidget {
  const AddReminderPage({Key key, this.onSubmitted}) : super (key: key);
  final Function({Reminder reminder}) onSubmitted;
  @override
  _AddReminderPageState createState() => _AddReminderPageState();
}

class _AddReminderPageState extends State<AddReminderPage> {
  final formKey = GlobalKey<FormState>();
  int _stepIndex = 0;
  bool _isError = false;
  String _title, _notes;
  DateTime _dateTime;
  final FocusNode taskFocus = FocusNode();
  final FocusNode notesFocus = FocusNode();
  final Color yellowTheme = Color(0xFFFABB18);


  @override
  void dispose() {
    taskFocus.dispose();
    notesFocus.dispose();
    super.dispose();
  }

  _updateStep(){
    setState(() {
      if(_stepIndex == 0)
        taskFocus.requestFocus();
      else if(_stepIndex == 1)
        notesFocus.requestFocus();
      else{
        taskFocus.unfocus();
        notesFocus.unfocus();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Form(
          key: formKey,
          child: Stepper(
            currentStep: _stepIndex,
            onStepTapped: (index) {
              _stepIndex = index;
              _updateStep();
            },
            controlsBuilder: (BuildContext context,
                {VoidCallback onStepContinue, VoidCallback onStepCancel}) => Container(),
            steps: [
            Step(
              title:Text((_title != null)?(_title.isNotEmpty)?_title:"Title":"Title"),
              subtitle: (_stepIndex!=0 && _title==null)
                  ?Text("Add a title for your task")
                  :null,
              content: TaskInput(
                focusNode: taskFocus,
                onNext: (){
                  _stepIndex = 1;
                  _updateStep();
                },
                updateValues: ({isError, title}) {
                  _title = title;
                  if(_isError!=isError){
                    setState(() {
                      _isError = isError ?? false;
                      if(_isError) _stepIndex =0;
                    });
                  }
                },
              ),
              state: (_isError)
                  ?StepState.error
                  :(_title != null)
                  ?(_title.isNotEmpty)
                  ?StepState.complete
                  :StepState.indexed
                  :StepState.indexed
            ),
            Step(
                title: Text('Notes'),
                subtitle: (_stepIndex==1)
                    ?null
                    :Text('Click to add a note'),
                content: NotesInput(
                  focusNode: notesFocus,
                  onNext: (){
                    _stepIndex = 2;
                    _updateStep();
                  },
                  notesValue: ({notes}) {
                    _notes = notes;
                  },
                ),
                state: StepState.editing,
            ),
            Step(
                title:Text('When?'),
                subtitle: Text('Add a date and time to remind you'),
                content: Container(
                  alignment: Alignment.centerLeft,
                  child: DateTimePicker(
                      dateTimeValue: ({dateTime, completed}) {
                        _dateTime = dateTime;
                        if(completed)
                          setState(() {});
                      },
                    ),
                ),
                state: (_dateTime != null)
                    ?StepState.complete
                    :StepState.indexed
            ),
          ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(bottom: 16, right: 32),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CustomTickIconButton(
                onPressed: () {
                  if(formKey.currentState.validate()){
                    formKey.currentState.save();
                    final Reminder reminder = Reminder(
                      title: _title,
                      notes: _notes,
                      dateTime: _dateTime??DateTime.now(),
                    );
                    print("${reminder.id} : "
                        "${reminder.title} : "
                        "${reminder.notes} : "
                        "${reminder.dateTime} : "
                        "${reminder.isCompleted} : ");

                    widget.onSubmitted(reminder: reminder);
                    Navigator.of(context).pop();
                  }
                },
              ),
            ],
          ),
        ),
      ],
    );
  }
}

class NotesInput extends StatefulWidget {
  const NotesInput({Key key,
    @required this.notesValue,
    @required this.focusNode,
    @required this.onNext
  }) : super(key:key);

  final Function({String notes}) notesValue;
  final FocusNode focusNode;
  final Function onNext;

  @override
  _NotesInputState createState() => _NotesInputState();
}

class _NotesInputState extends State<NotesInput> {
  final notesController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      style: Theme.of(context).textTheme.caption,
      controller: notesController,
      focusNode: widget.focusNode,
      maxLines: 3,
      decoration: InputDecoration(
        hintText: "Add a side note for your task..",
        suffixIcon: IconButton(
            icon: Icon(
              Icons.close,
              size: 15.0,
            ),
            onPressed: () {
              setState(() {
                notesController.clear();
              });
            }),
      ),
      onEditingComplete: () => widget.onNext(),
      textInputAction: TextInputAction.next,
      onSaved: (text){
        widget.notesValue(notes: text);
        print('Notes added : $text');
      },
    );
  }
}

class TaskInput extends StatefulWidget {

  const TaskInput({Key key,
    @required this.updateValues,
    this.initialValue,
    this.focusNode,
    @required this.onNext
  }) : super(key:key);

  final Function({String title, bool isError}) updateValues;
  final String initialValue;
  final FocusNode focusNode;
  final Function onNext;
  @override
  _TaskInputState createState() => _TaskInputState();
}

class _TaskInputState extends State<TaskInput> {
  int _selected = -1;
  final Color yellowTheme = Color(0xFFFABB18);
  final TextEditingController taskNameController = TextEditingController();
  final List<String> options = [
    'Call',
    'Text',
    'Do',
    'Pay',
    'Study',
    'Check',
    'Meet',
    'Attend',
    'Clean',
    'Attend class',
    'Assignment',
    'Make',
    'Finish',
    'Read',
    'Help'
  ];

  @override
  void initState() {
    super.initState();
    taskNameController.text = widget.initialValue;
    taskNameController.addListener(() {
      widget.updateValues(title: taskNameController.text);
    });
  }

  @override
  void dispose() {
    taskNameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var labels =
        List<Widget>.generate(options.length, (index) => Text(options[index]));
    return Column(
      children: [
        TextFormField(
          autofocus: true,
          focusNode: (widget.focusNode!=null)?widget.focusNode:null,
          controller: taskNameController,
          onFieldSubmitted: (text) {
            widget.updateValues(title: text);
          },
          cursorColor: yellowTheme,
          decoration: InputDecoration(
            hintText: "Remind me to..",
            suffixIcon: IconButton(
                icon: Icon(
                  Icons.close,
                  size: 15.0,
                ),
                onPressed: () {
                  setState(() {
                    taskNameController.clear();
                    widget.updateValues();
                    _selected = -1;
                  });
                }),
          ),
          onEditingComplete: () => widget.onNext(),
          textInputAction: TextInputAction.next,
          validator: (text){
            if(text.isEmpty) {
              widget.updateValues(isError: true);
              return "Please enter a task";
            }
            widget.updateValues(isError: false);
            return null;
          },
          onSaved: (text){
            widget.updateValues(title: text);
          },
        ),

        SizedBox(height: 12.0,),

        AnimatedSwitcher(
          duration: Duration(milliseconds: 200),
          transitionBuilder: (child, animation) {
            final  offsetAnimation =
            Tween<Offset>(begin: Offset(1.0, 0.0), end: Offset(0.0, 0.0)).animate(animation);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
          child: (taskNameController.text.isEmpty)?ScrollChips(
            selected: _selected,
            labels: labels,
            onPressed: (index) {
              if (index == -1)
                //No option selected
                taskNameController.clear();
              else {
                taskNameController.clear();
                var title = options[index];
                taskNameController.value = taskNameController.value.copyWith(
                    text: title + ' ',
                    selection: TextSelection(
                        baseOffset: title.length+1,
                        extentOffset: title.length+1
                    )
                );
                widget.updateValues(title: title);
              }
              setState(() {
                _selected = index;
              });
            },
          )
              :Container(),
          switchInCurve: Curves.easeInOut,
          switchOutCurve: Curves.easeInOut,
        ),
      ],
    );
  }
}

class DateTimePicker extends StatefulWidget {

  const DateTimePicker({Key key,
    @required this.dateTimeValue,
    this.initialValue
  }) : super(key:key);

  final Function({DateTime dateTime, bool completed}) dateTimeValue;
  final DateTime initialValue;

  @override
  _DateTimePickerState createState() => _DateTimePickerState();
}

class _DateTimePickerState extends State<DateTimePicker> {
  int _selectedDate = -1;
  String _dateTimeUI = "";
  int _selectedTime = -1;
  DateTime dateTime;
  var initialValue;

  @override
  void initState() {
    super.initState();
    initialValue = widget.initialValue;
  }

  @override
  Widget build(BuildContext context) {
    if(initialValue != null){
      _selectCustomDate(initialValue);
      dateTime = initialValue;
      _selectedTime = 3;
      _dateTimeUI+=' ' + TimeOfDay.fromDateTime(initialValue).format(context);
      widget.dateTimeValue(dateTime: dateTime, completed: false);
      initialValue = null;
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        AnimatedSwitcher(
          duration: Duration(milliseconds: 300),
          transitionBuilder: (child, animation) {
            final  offsetAnimation =
            Tween<Offset>(begin: Offset(-5.0, 0.0), end: Offset(0.0, 0.0)).animate(animation);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
          child: (_selectedTime >= 0)
              ?Container(
            height: 35,
            child: InputChip(
              backgroundColor: Colors.white,
              label: Text(_dateTimeUI),
              labelStyle: Theme.of(context).textTheme.caption,
              avatar: Icon(
                Icons.event_available,
                size: 15.0,
              ),
              shape: StadiumBorder(
                  side: BorderSide(
                    width: 1.0,
                  )),
              deleteIcon: Icon(
                Icons.close,
                size: 15.0,
              ),
              onDeleted: () {
                setState(() {
                  initialValue = null;
                  _resetDateTime();
                });
              },
            ),
          )
              :Padding(
            key: Key(_selectedDate.toString()),
            padding: const EdgeInsets.symmetric(vertical: 5.5),
            child: Text(
              (_selectedDate <0 )?"Date?":"Time?",
              style: Theme.of(context).textTheme.headline6,
            ),
          ),

          switchInCurve: Curves.easeInOut,
          switchOutCurve: Curves.easeInOut,
        ),

        SizedBox(height: 12.0,),

        AnimatedSwitcher(
          duration: Duration(milliseconds: 500),
          transitionBuilder: (child, animation) {
            final  offsetAnimation =
            Tween<Offset>(begin: Offset(10.0, 0.0), end: Offset(0.0, 0.0)).animate(animation);
            return SlideTransition(
              position: offsetAnimation,
              child: child,
            );
          },
          child: (_selectedDate <0 || _selectedTime >= 0)
              ?ScrollChips(
            key: Key('Dates'),
            selected: _selectedDate,
            labels: [
              Text("Today"),
              Text("Tomorrow"),
              Text("Custom"),
            ],
            onPressed: (index) {
              switch (index) {
                case 0:
                case 1:
                  _selectDate(index);
                  break;
                case 2:
                  showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime.now(),
                    lastDate: DateTime.now().add(Duration(days: 365)),
                  ).then((date) {
                    setState(() {
                      _selectCustomDate(date);
                    });
                  });
                  break;
                default:
                  _resetDateTime();
              }
              setState(() {});
            },
          )
              :ScrollChips(
            key: Key('Times'),
            selected: _selectedTime,
            labels: [
              Text("Morning"),
              Text("Afternoon"),
              Text("Evening"),
              Text("Custom"),
            ],
            onPressed: (index) {
              _selectedTime = index;
              switch (index) {
                case 0:
                  dateTime = dateTime.add(Duration(hours: 8));
                  _dateTimeUI += " 8:00AM";
                  break;
                case 1:
                  dateTime = dateTime.add(Duration(hours: 13));
                  _dateTimeUI += " 1:00PM";
                  break;
                case 2:
                  dateTime = dateTime.add(Duration(hours: 19));
                  _dateTimeUI += " 7:00PM";
                  break;
                case 3:
                  _selectedTime = -1;
                  showTimePicker(context: context, initialTime: TimeOfDay.now())
                      .then((time) {
                    if (time != null) {
                      print("$time");
                      dateTime = dateTime.add(Duration(hours: time.hour, minutes: time.minute));
                      print("$dateTime");
                      _selectedTime = index;
                      _dateTimeUI += '  ' + time.format(context);
                    }
                    setState(() {
                      widget.dateTimeValue(dateTime: dateTime, completed: true);
                    });
                  });
                  break;
                default:
                  throw Exception('Time is null');
              }
              setState(() {
                widget.dateTimeValue(dateTime: dateTime, completed: (_selectedTime!=null)?true:false);
              });
            },
          ),
          switchInCurve: Curves.easeInOut,
          switchOutCurve: Curves.easeInOut,
        ),
      ],
    );
  }

  void _selectDate(int index) {
    //Assuming index of Today is 0
    var today = DateTime.now();
    _resetDateTime();
    _selectedDate = index;
    _dateTimeUI = (index == 0)?"Today ":"Tomorrow ";
    ///Here is index in 1 (Tomorrow) it adds 1 to the date (next date)
    dateTime = DateTime(today.year,today.month, today.day + index);
    widget.dateTimeValue(dateTime: dateTime, completed: false);
  }

    void _selectCustomDate(DateTime date) {
    _resetDateTime();
    dateTime = DateTime(date.year, date.month, date.day);
    _selectedDate = 2;
    _dateTimeUI = DateFormat.yMMMMd('en_US').format(date);
    var _today = DateFormat.yMMMMd('en_US').format(DateTime.now());
    var _tomorrow = DateFormat.yMMMMd('en_US').format(DateTime.now().add(Duration(days: 1)));
    print(dateTime.toString());
    if(_today == _dateTimeUI) {
      _dateTimeUI = "Today";
      _selectedDate = 0;
    }
    else if(_dateTimeUI == _tomorrow) {
      _dateTimeUI = "Tomorrow";
      _selectedDate = 1;
    }
    widget.dateTimeValue(dateTime: dateTime, completed: false);
    }

  void _resetDateTime() {
    _selectedTime = -1;
    _selectedDate = -1;
    _dateTimeUI = '';
    if(initialValue==null)widget.dateTimeValue(completed: false); //Assign with null
  }
}

class ScrollChips extends StatelessWidget {
  const ScrollChips({
    Key key,
    @required this.selected,
    @required this.onPressed,
    @required this.labels,
  }) : super(key: key);

  final Function(int) onPressed;
  final List<Widget> labels;
  final int selected;

  @override
  Widget build(BuildContext context) {
    Color yellowTheme = Color(0xFFFABB18);
    var chips = List<Widget>.generate(
        labels.length,
        (index) => Padding(
              padding: const EdgeInsets.all(2.0),
              child: ChoiceChip(
                  selected: (selected == index) ? true : false,
                  shape: RoundedRectangleBorder(
//                    side: BorderSide(width: 1.0),
                      borderRadius: BorderRadius.all(Radius.circular(24.0),
                      ),
                  ),
                  backgroundColor:(selected!=-1)? yellowTheme.withAlpha(100):yellowTheme,
                  selectedColor: yellowTheme,
                  labelStyle: TextStyle(color: Colors.black),
                  label: labels[index],
                  onSelected: (_) {
                    if (selected == index)
                      onPressed(-1);
                    else
                      onPressed(index);
                  }),
            ));
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: chips,
      ),
    );
  }
}
