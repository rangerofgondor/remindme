import 'package:flutter/material.dart';
import 'package:remindme/models/model.dart';
import 'package:remindme/pages/common/widgets.dart';

import 'addReminder.dart';


class EditReminderPage extends StatefulWidget {
  const EditReminderPage({
    Key key,
    @required this.reminder,
    @required this.onSubmitted,
    this.onlyNotes
  }) : super (key: key);

  final Function({Reminder reminder}) onSubmitted;
  final Reminder reminder;
  final bool onlyNotes;
  @override
  _EditReminderPageState createState() => _EditReminderPageState();
}

class _EditReminderPageState extends State<EditReminderPage> {
  final formKey = GlobalKey<FormState>();
  String _title, _notes;
  DateTime _dateTime;
  bool isCompleted;
  final FocusNode taskFocus = FocusNode();
  final FocusNode notesFocus = FocusNode();


  @override
  void dispose() {
    taskFocus.dispose();
    notesFocus.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final reminder = widget.reminder;
    return Form(
        key: formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 36.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: (widget.onlyNotes==null)?TaskInput(
                  onNext: (){},
                  focusNode: taskFocus,
                  initialValue: reminder.title,
                  updateValues: ({isError,title}) {
                    if(_title != title){
                      setState(() {
                        _title = title;
                      });
                    }
                  },
                ):Text(reminder.title,
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 18.0,
                      color: Colors.black
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 8.0),
                child: (widget.onlyNotes==null)
                    ?EditNotesInput(
                  notesFocus: notesFocus,
                  notesValue: ({notes}) {
                    _notes = notes;
                  },
                  initialValue: reminder.notes,
                ):NotesInput(
                  onNext: (){},
                    notesValue: ({notes}) => _notes=notes,
                    focusNode: FocusNode()..requestFocus()
                ),
              ),
              (widget.onlyNotes==null)?Padding(
                padding: EdgeInsets.symmetric(vertical: 12.0),
                child: DateTimePicker(
                  initialValue: reminder.dateTime,
                  dateTimeValue: ({dateTime, completed}) {
                    _dateTime = dateTime;
                    if(completed)
                      setState(() {});
                  },
                ),
              ):Container(),
              (reminder.isCompleted && widget.onlyNotes==null)
                  ?Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: CustomCheckBox(
                  onChanged: ({checked}) => isCompleted=!checked,
                ),
              ):Container(),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 8, vertical: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(icon: Icon(Icons.clear),iconSize: 30, onPressed: (){
                      Navigator.of(context).pop();
                    }),
                    CustomTickIconButton(
                      onPressed: () {
                        if(formKey.currentState.validate()){
                          formKey.currentState.save();
                          final Reminder newReminder = Reminder(
                            id: reminder.id,
                            title: (widget.onlyNotes==null)
                                ?_title??''
                                :reminder.title,
                            notes: _notes??'',
                            dateTime: (widget.onlyNotes==null)
                                ?_dateTime??reminder.dateTime
                                :reminder.dateTime,
                            isCompleted: (widget.onlyNotes==null)
                                ?isCompleted??false
                                :reminder.isCompleted
                          );
                          print("${newReminder.id} : "
                              "${newReminder.title} : "
                              "${newReminder.notes} : "
                              "${newReminder.dateTime} : "
                              "${newReminder.isCompleted} : ");

                          widget.onSubmitted(reminder : newReminder);
                          Navigator.of(context).pop();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }
}

class CustomCheckBox extends StatefulWidget {

  const CustomCheckBox({Key key, @required this.onChanged}) : super(key: key);

  final Function({bool checked}) onChanged;
  @override
  _CustomCheckBoxState createState() => _CustomCheckBoxState();
}

class _CustomCheckBoxState extends State<CustomCheckBox> {
  bool checked;


  @override
  void initState() {
    super.initState();
    checked = true;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
          value: checked,
          onChanged: (value) {
            setState(() {
              checked = value;
              widget.onChanged(checked: checked);
            });
          },),
        Text('Mark as incomplete'),
      ],
    );
  }
}


class EditNotesInput extends StatefulWidget {
  const EditNotesInput({Key key,
    @required this.notesValue,
    @required this.initialValue,
    @required this.notesFocus
  }) : super(key:key);

  final Function({String notes}) notesValue;
  final FocusNode notesFocus;
  final String initialValue;

  @override
  _EditNotesInputState createState() => _EditNotesInputState();
}

class _EditNotesInputState extends State<EditNotesInput> {
  bool isPressed = false;
  String initialValue;
  final notesController = TextEditingController();
  final Color yellowTheme = Color(0xFFFABB18);

  @override
  void initState() {
    super.initState();
    initialValue = widget.initialValue??'';
    print('${widget.initialValue}');
    notesController.text = widget.initialValue;
  }


  @override
  void dispose() {
    notesController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(initialValue.isNotEmpty){
      isPressed = true;
    }
    return AnimatedCrossFade(
      duration: Duration(milliseconds: 300),
      crossFadeState: (isPressed)?CrossFadeState.showSecond:CrossFadeState.showFirst,
      firstChild: ActionChip(
        backgroundColor: yellowTheme,
        onPressed: (){
          setState(() {
            widget.notesFocus.requestFocus();
            isPressed = true;
          });
        },
        label: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Add a note  ',
              textAlign: TextAlign.start,
            ),
            Icon(Icons.add_circle, color: Colors.black,)
          ],
        ),
        labelStyle: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 14.0,
            color: Colors.black
        ),
      ),
      secondChild: TextFormField(
        focusNode: widget.notesFocus,
        controller: notesController,
        style: Theme.of(context).textTheme.caption,
        maxLines: 3,
        decoration: InputDecoration(
          hintText: "Add a side note for your task..",
          suffixIcon: IconButton(
              icon: Icon(
                Icons.close,
                size: 15.0,
              ),
              onPressed: () {
                setState(() {
                  isPressed = false;
                  initialValue = '';
                  widget.notesFocus.unfocus();
                  notesController.clear();
                });
              }),
        ),
        onSaved: (text){
          if(isPressed){
            widget.notesValue(notes: text);
            print('Notes added : $text');
          }

        },
      ),
    );
  }
}