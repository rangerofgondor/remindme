import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TabBarSliver extends SliverPersistentHeaderDelegate {
  TabBarSliver({
    @required this.minHeight,
    @required this.maxHeight,
    @required this.tabController,
    @required this.tabs,
  });
  final TabController tabController;
  final List<String> tabs;
  final double minHeight;
  final double maxHeight;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => math.max(maxHeight, minHeight);

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    Color yellowTheme = Color(0xFFFABB18);
    var tabLabels = List<Widget>.generate(tabs.length, (index) => Text(tabs[index],));
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(50.0),
            bottomRight: Radius.circular(50.0)
        ),
        boxShadow: [
          BoxShadow(
              color: Color(0x40000000),
              offset: Offset(2.0, 2.0),
              blurRadius: 30.0
          )
        ]
      ),
      child: Center(
        child: TabBar(
          isScrollable: true,
          labelStyle: Theme.of(context).textTheme.headline5.copyWith(
              fontSize: 18.0,
              fontWeight: FontWeight.w600,
              color: Colors.black
          ),
          unselectedLabelStyle: Theme.of(context).textTheme.headline5.copyWith(
              fontSize: 16.0,
              fontWeight: FontWeight.w300,
              color: Colors.black
          ),
          indicator: BoxDecoration(
              color: yellowTheme,
              borderRadius: BorderRadius.all(Radius.circular(25.0))
          ),
          labelPadding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 8.0),
          tabs: tabLabels,
          controller: tabController,
        ),
      ),
    );
  }

  @override
  bool shouldRebuild(TabBarSliver oldDelegate) {
    return maxHeight != oldDelegate.maxHeight ||
        minHeight != oldDelegate.minHeight;
  }
}

class TimeDisplay extends StatelessWidget {
  const TimeDisplay({Key key,@required this.time}) : super(key: key);
  final String time;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 80,
      child: Text(
        time,
        style: TextStyle(
            fontWeight: FontWeight.w300,
            fontSize: 14.0,
            color: Colors.black
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class CustomExpansionCard extends StatelessWidget {

  CustomExpansionCard({
    Key key,
    @required this.cardKey,
    @required this.titleRow,
    @required this.expandedSubtitleRow,
    @required this.isExpanded,
    @required this.onCompleted,
    @required this.onPressed,
    @required this.onLongPressed,
    @required this.onDeleted,
  }) : super(key: key);
  final List<Widget> titleRow;
  final List<Widget> expandedSubtitleRow;
  final bool isExpanded;
  final String cardKey;
  final Function onCompleted;
  final Function onPressed;
  final Function onLongPressed;
  final Function onDeleted;
  final Color yellowTheme = Color(0xFFFABB18);

  @override
  Widget build(BuildContext context) {
    return (!isExpanded)
        ?_commonChild(context, isExpanded)
        :_commonChild(context, isExpanded);
  }

  Widget _commonChild(BuildContext context, bool isExpanded) {
    //TOTAL Height : 60 + 10  = 70
    //TOTAL Expanded Height : 140 + 20  = 160
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, (isExpanded) ? 20.0 : 10.0),
      child: (isExpanded)
          ?Dismissible(
            key: Key(cardKey),
            child: _cardBuilder(),
            background: Slide(
              isComplete: onCompleted!=null,
              isLeft: true,
            ),
            secondaryBackground:Slide(),
            onDismissed: (direction) {
              if(direction == DismissDirection.startToEnd){
                ///Mark Complete ( Slide left to right )
                (onCompleted!=null)?onCompleted():onDeleted();
              }
              else{
                ///Delete ( Slide right to left )
                onDeleted();
              }
            },
          )
          :_cardBuilder(),
    );
  }

  Widget _cardBuilder() {
    return Card(
      elevation: (isExpanded)?10.0:0.0,
      margin: EdgeInsets.all(0),
      color: (isExpanded) ?  yellowTheme: null,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(15.0),
          )),
      child: Container(
        height: (isExpanded) ? 140 : 50,
        child: InkWell(
          splashColor: Color(0x33000000),
          focusColor: Colors.white,
          highlightColor: Color(0x55FFFFFF),
          onTap: () {
            onPressed();
          },
          onLongPress: () {
            onLongPressed();
          },
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 8.0, horizontal: 16),
            child: _expandedBuilder(isExpanded),
          ),
        ),
      ),
    );
  }

  Widget _expandedBuilder(bool isExpanded) {
    if (!isExpanded) {
      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: titleRow);
    } else {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Spacer(
            flex: 1,
          ),
          Flexible(
            flex: 4,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: titleRow
            ),
          ),
          Spacer(
            flex: 1,
          ),
          Expanded(
            flex: 8,
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: expandedSubtitleRow),
          ),
          Spacer(
            flex: 1,
          ),
        ],
      );
    }
  }
}

class CustomTickIconButton extends StatelessWidget {
  const CustomTickIconButton ({
    Key key,
    @required this.onPressed,
  }):super(key: key);

  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 45,
      width: 45,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          color: Colors.black),
      child: IconButton(
        iconSize: 30,
        icon: Icon(Icons.check),
        color: Colors.white60,
        onPressed: onPressed,
      ),
    );
  }
}

class CustomBottomSheet{
  final BuildContext context;
  final Widget child;
  final String title;

  CustomBottomSheet.of({
    @required this.context,
    @required this.child,
    @required this.title
  });

  show(){
    showModalBottomSheet(
      shape: RoundedRectangleBorder(
          borderRadius:
          BorderRadius.vertical(top: Radius.circular(25.0))),
      context: context,
      isScrollControlled: true,
      builder: (context) => SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 36.0, top: 36.0),
                  child: Text(title,
                    textAlign: TextAlign.start,
                    style: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: 24.0,
                        color: Colors.black
                    ),
                  ),
                ),
                child
              ],
            ),
          )),
    );
  }
}

class Slide extends StatefulWidget {
  const Slide({Key key, this.isComplete=false, this.isLeft=false}):super(key: key);

  final bool isComplete;
  final bool isLeft;

  @override
  _SlideState createState() => _SlideState();
}

class _SlideState extends State<Slide>{

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        alignment: (widget.isLeft)?Alignment.centerLeft:Alignment.centerRight,
        children: [
          Container(
            height: 120.0,
            width: 120.0,
            decoration: BoxDecoration(
              color: (widget.isComplete && widget.isLeft)?Colors.green:Colors.red,
              shape: BoxShape.circle
            ),
          ),
          Positioned(
            left: (widget.isLeft)?60.0:null,
            right: (!widget.isLeft)?60.0:null,
            child: Container(
              height: 120.0,
              width: 300.0,
              decoration: BoxDecoration(
                  color: (widget.isComplete && widget.isLeft)?Colors.green:Colors.red,
              ),
            ),
          ),
          Positioned(
            left:(widget.isLeft)?30.0:null,
            right:(!widget.isLeft)?30.0:null,
            child: Icon((widget.isComplete)?Icons.done:Icons.delete,
              color: Colors.white,
              size: 40,
            ),
          ),
          Positioned(
            left: (widget.isLeft)?80.0:null,
            right: (!widget.isLeft)?80.0:null,
            child: Text(
              (widget.isComplete && widget.isLeft)?'Complete':'Delete',
              style: Theme.of(context).textTheme.bodyText1.copyWith(
                  color: Colors.white
              ),
            ),
          ),
        ],
      ),
    );
  }
}