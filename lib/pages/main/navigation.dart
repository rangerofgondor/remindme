import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/bloc/state.dart';
import 'package:remindme/notifications/notifier.dart';
import 'package:remindme/pages/add_edit/addReminder.dart';
import 'package:remindme/pages/all/all.dart';
import 'package:remindme/pages/common/widgets.dart';
import 'package:remindme/pages/home/home.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with SingleTickerProviderStateMixin{
  TabController tabController;
  final Color yellowTheme = Color(0xFFFABB18);

  @override
  void initState() {
    super.initState();
    tabController = TabController(length: 2, vsync: this);
    ///Initialise Local Notifications
    NotificationManager(context: context).initialise();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Color(0x11000000),
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark
    ));
    BlocProvider.of<ReminderBloc>(context).add(EventAppStart());
    return Scaffold(

      body: BlocListener<ReminderBloc,ReminderState>(
          child: BlocListener<ReminderBloc,ReminderState>(
              child: TabBarView(
                controller: tabController,
                children: [
                  HomePage(),
                  AllPage(),
                ],
              ),
              condition: (previous, current) => (current is StateFocusMain),
              listener:(context, state) {
                tabController.animateTo((state as StateFocusMain).tabIndex);
              }
          ),
          condition: (previous, current) => (current is StateShowSnackBar),
          listener:(context, state) {
            if(state is StateShowSnackBar)
              Scaffold.of(context).showSnackBar(
                SnackBar(
                    action: (state.withUndo)
                        ?SnackBarAction(label: 'Undo', onPressed: state.onUndo)
                        :null,
                    content: Text(state.message,)
                ),
              );
          }
      ),
        bottomNavigationBar: CustomBottomNav(
          tabController: tabController,
        ),
      floatingActionButton: Builder(builder: (parent) {
        return FloatingActionButton.extended(
          elevation: 8.0,
          label: Row(
            children: [
              Text(
                "Add",
                style: Theme.of(parent)
                    .textTheme
                    .headline6
                    .copyWith(
                    color: Colors.white,
                  fontWeight: FontWeight.w400
                ),
              ),
              SizedBox(width: 10,),
              Icon(Icons.add_circle,
                color: yellowTheme,
              ),
            ],
          ),
          backgroundColor: Colors.black,
          onPressed: (){
            CustomBottomSheet.of(
              context: context,
              title: 'Add a Reminder',
              child: AddReminderPage(
                onSubmitted: ({reminder})
                => BlocProvider.of<ReminderBloc>(parent).add(EventAdd(reminder: reminder)),
              ),
            ).show();
          },
        );
      }),
    );
  }
}

class CustomBottomNav extends StatefulWidget {
  const CustomBottomNav({
    Key key,
    this.tabController
  }):super(key: key);

  final TabController tabController;
  @override
  _CustomBottomNavState createState() => _CustomBottomNavState();
}

class _CustomBottomNavState extends State<CustomBottomNav> {
  int current = 0;
  final Color yellowTheme = Color(0xFFFABB18);
  CircularBottomNavigationController _bottomNavController;

  @override
  void initState() {
    super.initState();
    _bottomNavController = CircularBottomNavigationController(0);
    widget.tabController.addListener(() {
      _bottomNavController.value = widget.tabController.index;
    });
  }

  @override
  void dispose() {
    _bottomNavController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        Container(
          height: 70.0,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50.0),
                  topRight: Radius.circular(50.0)
              ),
              boxShadow: [
                BoxShadow(
                    color: Color(0x40000000),
                    offset: Offset(2.0, 2.0),
                    blurRadius: 30.0
                )
              ]
          ),
        ),
        ClipRect(
          child: Align(
            alignment: Alignment.bottomCenter,
            widthFactor: 0.65,
            heightFactor: 1.0,
            child: CircularBottomNavigation(
                [
                  TabItem(Icons.home, 'Home', yellowTheme),
                  TabItem(Icons.event_note, 'All', yellowTheme),
                ],
                selectedIconColor: Colors.black,
                controller: _bottomNavController,
                selectedPos: current,
                barHeight: 70.0,
                selectedCallback: (selectedPos) {
                  widget.tabController.animateTo(selectedPos);
                  setState(() {
                    current = selectedPos;
                  });
                }
            ),
          ),
        ),
      ],
    );
  }
}
