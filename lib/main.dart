import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/bloc_delegate.dart';
import 'package:remindme/one_time/intro_main.dart';
import 'package:remindme/one_time/splash.dart';
import 'package:remindme/pages/main/navigation.dart';

import 'one_time/intro_home.dart';
//import 'package:flutter_localizations/flutter_localizations.dart';

///Instructions to create App Icon:
/// - flutter packages get
/// - flutter packages pub run flutter_launcher_icons:main

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    BlocSupervisor.delegate = SimpleBlocDelegate();
    final Color yellowTheme = Color(0xFFFABB18);
    return MaterialApp(
      // TODO: localizations are implemented, but we still have to use it in main place, therefore commented as of now.
//      localizationsDelegates: [
//        GlobalMaterialLocalizations.delegate,
//        GlobalWidgetsLocalizations.delegate,
//      ],
//      supportedLocales: [
//        const Locale('en', 'EN'), // English
//        const Locale('hi', 'HI'), // Hindi
//        const Locale('ja', 'JA'), // Japanese
//        const Locale('ka', 'KA'), // Kannada,
//      ],
      title: 'RemindMe',
      theme: ThemeData(
        primaryColor: yellowTheme,
        accentColor: Colors.black,
        colorScheme: ColorScheme.light(
          primary: yellowTheme,
          onPrimary: Colors.black,
        ),
      ),
      initialRoute: '/',
      /*routes: {
        '/': (BuildContext context) => SplashScreen(),
        '/intro': (BuildContext context) => IntroHome(),
        '/intro/main': (BuildContext context) => IntroMain(),
        '/intro/first': (BuildContext context) => IntroFirst(),
        '/intro/second': (BuildContext context) => IntroSecond(),
        '/intro/third': (BuildContext context) => IntroThird(),
        '/intro/fourth': (BuildContext context) => IntroFourth(),
        '/intro/fifth': (BuildContext context) => IntroFifth(),
        '/intro/sixth': (BuildContext context) => IntroSixth(),
        '/main': (BuildContext context) => BlocProvider(
            create: (context) => ReminderBloc(),
            child: MainPage(),
        ),
        '/main/home': (BuildContext context) => HomePage(),
        '/main/add': (BuildContext context) => AddReminderPage(),
      },*/
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/':
            return CustomRoute(widget: SplashScreen());
            break;
          case '/intro':
            return CustomRoute(widget: IntroHome());
            break;
          case '/intro/main':
            return CustomRoute(widget: IntroMain());
            break;
          case '/main':
            return CustomRoute(widget: BlocProvider(
              create: (context) => ReminderBloc(),
              child: MainPage(),
            ));
            break;
          default:
            return CustomRoute(widget: SplashScreen());
        }
      },
    );
  }
}

class CustomRoute extends PageRouteBuilder {
  Widget widget;
  CustomRoute({this.widget,})
      : super(
      pageBuilder: (context, animation, secondaryAnimation) => widget,
      transitionsBuilder:
          (context, animation, secondaryAnimation, child) {
        var tween, curve, animationWithCurve;
          tween = Tween(
            begin: Offset(1.0, 0.0),
            end: Offset.zero,
          );
          curve = Curves.easeOut;
          animationWithCurve = CurvedAnimation(
            parent: animation,
            curve: curve,
          );
          return SlideTransition(
            position: tween.animate(animationWithCurve),
            child: child,
          );
      });
}