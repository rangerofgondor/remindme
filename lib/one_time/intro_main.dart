import 'package:flutter/material.dart';
import 'package:remindme/one_time/intro_first_set.dart';
import 'package:remindme/one_time/intro_second_set.dart';

class IntroMain extends StatefulWidget {
  @override
  _IntroMainState createState() => _IntroMainState();
}

class _IntroMainState extends State<IntroMain> with SingleTickerProviderStateMixin{
  TabController controller;


  @override
  void initState() {
    super.initState();
    controller = TabController(length: 6, vsync: this);
  }


  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Color yellowTheme = Color(0xFFFABB18);
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        IntroBackground(
          next: () => next(),
          back: () => back(),
        ),
        TabBarView(
            children: [
              IntroFirst(),
              IntroSecond(),
              IntroThird(),
              IntroFourth(),
              IntroFifth(),
              IntroSixth(),
            ],
          controller: controller,
        ),
        Positioned(
          bottom: 30.0,
          child: Row(
            children: [
              FloatingActionButton(
                heroTag: null,
                child: Container(
                  child: Icon(Icons.arrow_back_ios),
                ),
                foregroundColor: yellowTheme,
                backgroundColor: Colors.white,
                onPressed: () => back(),
              ),
              SizedBox(width: 100,),
              FloatingActionButton(
                heroTag: 'next',
                child: Container(
                  child: Icon(Icons.arrow_forward_ios),
                ),
                foregroundColor: yellowTheme,
                backgroundColor: Colors.white,
                onPressed: () => next(),
              ),
            ],
          ),
        ),
      ],
    );
  }

  next() {
    var current = controller.index;
    if(current < 5){
      controller.animateTo(current+1, curve: Curves.easeInOut,duration: Duration(milliseconds: 300));
    }
    else{
      Navigator.of(context).pushNamedAndRemoveUntil('/main', (route) => false);
    }
  }
  back() {
    var current = controller.index;
    if(current > 0){
      controller.animateTo(current-1, curve: Curves.easeInOut,duration: Duration(milliseconds: 300));
    }
    else{
      Navigator.of(context).pop();
    }
  }
}
