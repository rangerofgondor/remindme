import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
final Color yellowTheme = Color(0xFFFABB18);

class IntroBackground extends StatelessWidget {
  const IntroBackground({
    Key key,
    @required this.next,
    @required this.back,
    }):super(key: key);

  final Function next;
  final Function back;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Hero(
                tag: 'icon',
                child: Image.asset(
                  'assets/launcher/app-icon.png',
                  width: 50,
                  height: 50,
                ),
              ),
              SizedBox(width: 20,),
              Text(
                "Remind",
                style: TextStyle(
                    fontSize: 36.0,
                    fontWeight: FontWeight.w200,
                    color: Colors.black,
                ),
              ),
              Text(
                "Me",
                style: TextStyle(
                    fontSize: 36.0,
                    fontWeight: FontWeight.w900,
                    color: yellowTheme
                ),
              ),
            ],
          ),
          Spacer(flex: 8,),
          Stack(
            alignment: Alignment.center,
            children: <Widget>[
              CustomPaint(
                painter: BottomWaves(),
                child: Container(
                  height: 150.0,
                  width: MediaQuery.of(context).size.width,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class BottomWaves extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    
    //Bottom Curve :
    var paint = Paint()
      ..color = yellowTheme
      ..style = PaintingStyle.fill;

    var path = Path()
      ..moveTo(0, size.height)
      ..lineTo(0, 0)
      ..quadraticBezierTo(size.width*0.125, size.height*0.65, size.width*0.25, size.height * 0.25)
      ..quadraticBezierTo(size.width*0.375, -size.height*0.25, size.width*0.5, size.height * 0.25)
      ..quadraticBezierTo(size.width*0.625, size.height*0.65, size.width*0.75, size.height * 0.25)
      ..quadraticBezierTo(size.width*0.875, -size.height*0.25, size.width, size.height * 0.25)
      ..lineTo(size.width , size.height)
      ..close();
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}

class IntroFirst extends StatelessWidget {
  const IntroFirst({Key key,}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 32.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text(
                  "Convinient",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w600,
                      color: Colors.black
                  ),
                ),
                Text(
                  "Reminder",
                  style: TextStyle(
                      fontSize: 42,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "and",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w600,
                      color: Colors.black
                  ),
                ),
                Text(
                  "Day Planner",
                  style: TextStyle(
                      fontSize: 42,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 32.0, bottom: 16.0),
            child: Text(
              "Plan your day by adding reminders,"
                  " or schedule notifications for later.",
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w200,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 180,),
        ],
      ),
    );
  }
}

class IntroSecond extends StatelessWidget {
  const IntroSecond({Key key,}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "Add ",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
                Text(
                  "Reminder",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          FloatingActionButton.extended(
            onPressed: null,
            elevation: 8.0,
            label: Row(
              children: [
                Text(
                  "Add",
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w400
                  ),
                ),
                SizedBox(width: 10,),
                Icon(Icons.add_circle,
                  color: yellowTheme,
                ),
              ],
            ),
            backgroundColor: Colors.black,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Look for this button to add a reminder",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  color: Colors.black
              ),
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "- Add a Title\n"
                  "- Add an optional Note\n"
                  "- Select Date and Time to Remind.",
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w200,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 180,),
        ],
      ),
    );
  }
}

class IntroThird extends StatelessWidget {
  const IntroThird({Key key,}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "Home ",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "Page",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          CircularBottomNavigation(
              [
                TabItem(Icons.home, 'Home', yellowTheme),
                TabItem(Icons.event_note, 'All', yellowTheme),
              ],
              selectedIconColor: Colors.black,
              selectedPos: 0,
              barHeight: 40.0,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Find all your tasks for today and tomrrow here",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w200,
                  color: Colors.black
              ),
            ),
          ),
          Spacer(flex: 2,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "All ",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "Page",
                  style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
          CircularBottomNavigation(
            [
              TabItem(Icons.home, 'Home', yellowTheme),
              TabItem(Icons.event_note, 'All', yellowTheme),
            ],
            selectedIconColor: Colors.black,
            selectedPos: 1,
            barHeight: 40.0,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Find your upcoming, overdue and completed tasks here.",
              style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w200,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 150,),
        ],
      ),
    );
  }
}