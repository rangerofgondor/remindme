import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

final Color yellowTheme = Color(0xFFFABB18);

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Center(
            child: Image.asset(
              'assets/launcher/app-icon.png',
              height: 100.0,
              width: 100.0,
            ),
          ),
          Positioned(
            bottom: 100.0,
            child: Row(
              children: [
                Text(
                  "Remind",
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(fontWeight: FontWeight.w200, color: Colors.black),
                ),
                Text(
                  "Me",
                  style: Theme.of(context)
                      .textTheme
                      .headline4
                      .copyWith(fontWeight: FontWeight.w900, color: yellowTheme),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    ///Check for ONE TIME Intro screen
    Timer(
        Duration(milliseconds: 1500), () {
      checkFirstSeen();
    });
  }

  Future checkFirstSeen() async {
    Navigator.of(context).pushNamedAndRemoveUntil('/intro', (route) => false);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool _seen = (prefs.getBool('RemindMeIntro') ?? false);

    if (_seen) {
      Navigator.of(context).pushNamedAndRemoveUntil('/main',(route) => false);
    } else {
      prefs.setBool('RemindMeIntro', true);
      Navigator.of(context).pushNamedAndRemoveUntil('/intro', (route) => false);
    }
  }
}
