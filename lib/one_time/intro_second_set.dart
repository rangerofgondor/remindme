import 'package:flutter/material.dart';

final Color yellowTheme = Color(0xFFFABB18);

class IntroFourth extends StatelessWidget {
  const IntroFourth({Key key,}):super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "Complete ",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "Reminder",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Image.asset(
            'images/complete_sample.jpg',
            height: 150,
          ),
          Spacer(),
          Image.asset(
            'images/arrow-swipe-right.png',
            height: 50,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Swipe right to complete a task",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 180,),
        ],
      ),
    );
  }
}

class IntroFifth extends StatelessWidget {
  const IntroFifth({Key key,}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "Delete ",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "Reminder",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Image.asset(
            'images/delete_sample.jpg',
            height: 150,
          ),
          Spacer(),
          Image.asset(
            'images/arrow-swipe-left.png',
            height: 50,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Swipe left to delete a task",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 180,),
        ],
      ),
    );
  }
}

class IntroSixth extends StatelessWidget {
  const IntroSixth({Key key,}):super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0x000000),
      body: Column(
        children: [
          SizedBox(height: 150,),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Row(
              children: [
                Text(
                  "Edit ",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: yellowTheme
                  ),
                ),
                Text(
                  "Reminder",
                  style: TextStyle(
                      fontSize: 32,
                      fontWeight: FontWeight.w900,
                      color: Colors.black
                  ),
                ),
              ],
            ),
          ),
          Spacer(),
          Image.asset(
            'images/edit_sample.jpg',
            height: 150,
          ),
          Spacer(),
          Image.asset(
            'images/edit_icon.png',
            height: 50,
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(left: 16.0),
            child: Text(
              "Long Press to edit a task",
              style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.w300,
                  color: Colors.black
              ),
            ),
          ),
          SizedBox(height: 180,),
        ],
      ),
    );
  }
}