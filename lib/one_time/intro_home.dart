import 'dart:ui';

import 'package:flutter/material.dart';

final Color yellowTheme = Color(0xFFFABB18);

class IntroHome extends StatelessWidget {

  @override
  Widget build(BuildContext context){
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
              color: Colors.white
          ),
          Center(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(
                  0, 100, 0, 0
              ),
              child: Column(
                children: <Widget>[
                  Text(
                    "Welcome",
                    style: TextStyle(
                        fontSize: 52.0,
                        fontWeight: FontWeight.w200,
                        color: Colors.black
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(
                        0, 32, 0, 0
                    ),
                    child: Hero(
                      tag: 'icon',
                      child: Image.asset('assets/launcher/app-icon.png',
                        width: 100,
                        height: 100,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          CustomPaint(
            painter: HomePaint(),
            child: Container(
              color: Colors.black.withOpacity(0),
            ),
          ),
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.fromLTRB(
            0,0,0,32
        ),
        child: FloatingActionButton(
          heroTag: 'next',
          child: Container(
            child: Icon(Icons.arrow_forward_ios, color: Colors.white,),
          ),
          backgroundColor: yellowTheme,
          onPressed: () {
            Navigator.of(context).pushNamed('/intro/main');
          },
        ),
      ),
      floatingActionButtonLocation:
      FloatingActionButtonLocation.centerDocked,
    );
  }
}

class HomePaint extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {

    //1st Curve :
    var paint = Paint()
      ..color = Color(0xFFDC9F00)
      ..style = PaintingStyle.fill;

    var path = Path()
      ..moveTo(size.width, size.height * 0.25)
      ..quadraticBezierTo(size.width*0.65, size.height*0.6, 0, size.height*0.5)
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..close();
    canvas.drawPath(path, paint);


    //1st Curve :
    paint = Paint()
      ..color = Color(0xFFECAA00)
      ..style = PaintingStyle.fill;

    path = Path()
      ..moveTo(size.width, size.height * 0.3)
      ..quadraticBezierTo(size.width*0.65, size.height*0.7, 0, size.height*0.55)
      ..lineTo(0, size.height)
      ..lineTo(size.width, size.height)
      ..close();
    canvas.drawPath(path, paint);

    //3rd Curve :

    paint = Paint()
      ..color = yellowTheme
      ..style = PaintingStyle.fill;
    path = Path()
      ..moveTo(0, size.height * 0.45)
      ..quadraticBezierTo(size.width*0.5, size.height*0.85, size.width, size.height*0.65)
      ..quadraticBezierTo(size.width*0.5, size.height*0.9, 0, size.height*0.8)
      ..close();
    canvas.drawPath(path, paint);

    //Final bottom white
    paint = Paint()
      ..color = Color(0xFFFFFFFF)
      ..style = PaintingStyle.fill;

    path = Path()
      ..moveTo(0, size.height)
      ..lineTo(0, size.height * 0.80)
      ..quadraticBezierTo(size.width*0.5, size.height*0.9, size.width, size.height*0.65)
      ..lineTo(size.width, size.height)
      ..close();
    canvas.drawPath(path, paint);

  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    // TODO: implement shouldRepaint
    return false;
  }
}
