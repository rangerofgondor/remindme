import 'package:equatable/equatable.dart';

class Reminder extends Equatable {
  final int id;
  final String title;
  final String notes;
  final DateTime dateTime;
  final bool isCompleted;

  //Empty Constructor:
  Reminder({
    this.title = '',
    this.notes = '',
    this.dateTime,
    this.isCompleted = false,
    this.id,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'title': title,
      'notes': notes,
      'dateTime': dateTime.toIso8601String(),
      'isCompleted': (isCompleted)?1:0
    };
  }

  Reminder fromMap(dynamic obj) {
    return Reminder(
      id : obj['id'] as int,
      title : obj['title'],
      notes : obj['notes'],
      dateTime : DateTime.parse(obj['dateTime']),
      isCompleted : obj['isCompleted']==0?false:true,
    );
  }

  Reminder copyWith(
      {reminder, title, notes, time, date, dateTime, isCompleted}) {
    return Reminder(
      id: this.id,
      title: title ?? this.title,
      notes: notes ?? this.notes,
      dateTime: dateTime ?? this.dateTime,
      isCompleted: isCompleted ?? this.isCompleted,
    );
  }

  @override
  List<Object> get props => [this.id];
}

class GroupedReminders extends Equatable {
  final List<Reminder> reminders;
  final String groupTitle;

  const GroupedReminders({
    this.reminders,
    this.groupTitle,
  });

  GroupedReminders copyWith({
    List<Reminder> reminders,
    String groupTitle,
  }) {
    return GroupedReminders(
      reminders: reminders ?? this.reminders,
      groupTitle: groupTitle ?? this.groupTitle,
    );
  }

  @override
  List<Object> get props => [reminders, groupTitle];
}
