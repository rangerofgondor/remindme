import 'package:intl/intl.dart';
import 'package:remindme/database/database_dao.dart';
import 'package:remindme/models/model.dart';

///This class is to help retrieve and arrange the data stored in the database
///For now it uses a static variable for storage
class Repository {
  final remDao = RemDao();
  Future<List<Reminder>> getAllReminders() => remDao.getAllRems();
  Future<List<Reminder>> getTodayRems() => remDao.getTodayRems();
  Future<List<Reminder>> getTomorrowRems() => remDao.getTomorrowRems();
  Future<int> insertReminder(Reminder reminder) => remDao.insertRem(reminder);
  Future updateReminder(Reminder reminder) => remDao.updateRem(reminder);
  Future<Reminder> markReminderComplete(int id) => remDao.markCompletedById(id, 1);
  Future<Reminder> markReminderInComplete(int id) => remDao.markCompletedById(id, 0);
  Future<Reminder> deleteReminderById(int id) => remDao.deleteRem(id);
  Future deleteAllReminders() => remDao.deleteTbl();

  Future<List<Reminder>> get all async {
    List<Reminder> _reminders = await getAllReminders();
    _reminders.sort((a, b) => a.dateTime.compareTo(b.dateTime));
    return _reminders;
  }

  Future<List<Reminder>> get today async {
    List<Reminder> _reminders = await getTodayRems();
    _reminders.sort((a, b) => a.dateTime.compareTo(b.dateTime));
    return _reminders;
  }

  Future<List<Reminder>> get tomorrow async {
    List<Reminder> _reminders = await getTomorrowRems();
    _reminders.sort((a, b) => a.dateTime.compareTo(b.dateTime));
    return _reminders;
  }

  Future<List<Reminder>> get overdue async {
    final _allReminders = await getAllReminders();
    List<Reminder> overdueReminders = [];
    if(_allReminders.isNotEmpty) {
      _allReminders.sort((a, b) => a.dateTime.compareTo(b.dateTime));
      var _date = DateTime.now();
      for (var reminder in _allReminders) {
        if (!reminder.isCompleted && reminder.dateTime.isBefore(_date)) {
          overdueReminders.add(reminder);
        }
      }
    }
    return overdueReminders;
  }

  Future<Map<String, List<GroupedReminders>>> getAll() async {
    final _allReminders = await getAllReminders();

    if(_allReminders.isNotEmpty){
      _allReminders.sort((a,b) => a.dateTime.compareTo(b.dateTime));
      List<Reminder> upComingReminders = [];
      List<Reminder> overdueReminders = [];
      List<Reminder> completedReminders = [];
      var _date = DateTime.now();
      for (var reminder in _allReminders){
        if(reminder.isCompleted){
          completedReminders.add(reminder);
        }
        else if(reminder.dateTime.isAfter(_date)){
          upComingReminders.add(reminder);
        }
        else {
          overdueReminders.add(reminder);
        }
      }
      return {
        'upcoming' : await getAsGroup(upComingReminders),
        'overdue' : await getAsGroup(overdueReminders),
        'completed' : await getAsGroup(completedReminders),
      };
    }
    return {
      'upcoming' : [],
      'overdue' : [],
      'completed' : [],
    };
  }

  Future<List<GroupedReminders>> getAsGroup(List<Reminder> reminders) async{
    List<GroupedReminders> _group = [];
    List<Reminder> _filteredReminders = [];
    int curMonth, curYear, i = 0;

    DateFormat format = DateFormat('MMMM, y'); //It shows as : April, 2020
    print(
        'Reminders length : ${reminders.length} Group length : ${_group.length}');
    while (i < reminders.length) {
      curMonth = reminders[i].dateTime.month;
      curYear = reminders[i].dateTime.year;
      _filteredReminders = [];
      while (i < reminders.length &&
          reminders[i].dateTime.month == curMonth &&
          reminders[i].dateTime.year == curYear) {
        _filteredReminders.add(reminders[i]);
        i++;
      }
      _group.add(GroupedReminders(
          groupTitle: format.format(reminders[i - 1].dateTime),
          reminders: _filteredReminders));
    }

    return _group;
  }

  //Updating the value in Database:

  Future<int> addRem(Reminder reminder) async {
    return await insertReminder(reminder);
  }

  Future<String> markComplete(int id) async {
    var reminder = await markReminderComplete(id);
    return reminder.title;
  }

  Future<Reminder> markInComplete(int id) async {
    return await markReminderInComplete(id);
  }

  Future<Reminder> delete(int id) async {
    return await deleteReminderById(id);
  }

  Future edit(Reminder reminder) async {
    await updateReminder(reminder);
  }
}