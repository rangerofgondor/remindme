import 'package:equatable/equatable.dart';

abstract class ExpansionState extends Equatable {
  final int index;
  const ExpansionState({this.index});

  @override
  List<Object> get props => [];
}

class EStateExpand extends ExpansionState {
  final int index;

  const EStateExpand({
    this.index
  });

  @override
  List<Object> get props => [index];

  @override
  String toString() =>
      'EStateExpand $index,';
}

class EStateCollapse extends ExpansionState {
  final int index;

  const EStateCollapse({
    this.index
  });

  @override
  List<Object> get props => [index];

  @override
  String toString() =>
      'EStateCollapse $index,';
}