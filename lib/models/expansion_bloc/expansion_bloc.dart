import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remindme/models/expansion_bloc/events.dart';
import 'package:remindme/models/expansion_bloc/states.dart';

class ExpansionBloc extends Bloc<ExpansionEvent, ExpansionState> {

  int previousIndex = 0;
  @override
  ExpansionState get initialState => EStateCollapse(index: -1);

  @override
  Stream<ExpansionState> mapEventToState(ExpansionEvent event) async* {
    if (event is EEventPressed)
      yield _mapPressedToState(event);
    else if (event is EEventCollapse)
      yield _mapCollapsedToState(event);
    else if (event is EEventExpand)
      yield _mapExpandToState(event);
    else
      throw Exception("EventNotFound");
  }

  ExpansionState _mapPressedToState(EEventPressed event) {
    if(previousIndex != event.index){
      add(EEventCollapse(index: previousIndex));
      previousIndex = event.index;
    }
    return EStateExpand(index: event.index);
  }

  ExpansionState _mapCollapsedToState(EEventCollapse event) {
    return EStateCollapse(index: event.index);
  }
  ExpansionState _mapExpandToState(EEventExpand event) {
    return EStateExpand(index: event.index);
  }
}