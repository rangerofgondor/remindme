import 'package:equatable/equatable.dart';

abstract class ExpansionEvent extends Equatable {
  const ExpansionEvent();

  @override
  List<Object> get props => [];
}

class EEventPressed extends ExpansionEvent {
  final int index;
  const EEventPressed({
    this.index
  });

  @override
  List<Object> get props => [index];

  @override
  String toString() => 'EEventPressed - index : $index';
}

class EEventExpand extends ExpansionEvent {
  final int index;
  const EEventExpand({
    this.index
  });

  @override
  List<Object> get props => [index];

  @override
  String toString() => 'EEventExpand - index : $index';
}

class EEventCollapse extends ExpansionEvent {
  final int index;
  const EEventCollapse({
    this.index
  });

  @override
  List<Object> get props => [index];

  @override
  String toString() => 'EEventCollapse - index : $index';
}