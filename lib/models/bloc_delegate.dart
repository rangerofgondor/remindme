import 'package:bloc/bloc.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onTransition(Bloc bloc, Transition transition) {
    print('$bloc $transition');
    super.onTransition(bloc, transition);
  }
}