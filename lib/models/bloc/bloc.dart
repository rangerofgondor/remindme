import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:remindme/models/bloc/event.dart';
import 'package:remindme/models/bloc/state.dart';
import 'package:remindme/models/repository/repository.dart';
import 'package:remindme/notifications/notifier.dart';

class ReminderBloc extends Bloc<ReminderEvent, ReminderState> {
  Repository _repository = Repository();
  @override
  ReminderState get initialState => StateLoading();

  @override
  Stream<ReminderState> mapEventToState(ReminderEvent event) async* {
    if (event is EventAppStart)
      yield await _mapStartToState(event);
    else if (event is EventLoading)
      yield StateLoading();
    else if (event is EventHome)
      yield await _mapHomeToState(event);
    else if (event is EventAll)
      yield await _mapAllToState(event);
    else if (event is EventAdd)
      yield await _mapAddToState(event);
    else if (event is EventEdit)
      yield await _mapEditToState(event);
    else if (event is EventCompleted)
      yield await _mapCompletedToState(event);
    else if (event is EventDelete)
      yield await _mapDeleteToState(event);
    else if (event is EventSnackBar)
      yield _mapSnackBarToState(event);
    else if (event is EventChangeMainFocus)
      yield _mapFocusMainToState(event);
    else if (event is EventChangeFineFocus)
      yield await _mapFocusFineToState(event);
    else
      throw Exception("EventNotFound");
  }

  Future<ReminderState> _mapStartToState(EventAppStart event) async{
    var overdueRem = await _repository.overdue;
    if(overdueRem.length > 0){
      add(EventChangeFineFocus(fineFocus: focus.overdue));
      return StateFocusMain(tabIndex: 1);
    }
    return StateFocusMain(tabIndex: 0);
  }

  Future<ReminderState> _mapHomeToState(ReminderEvent event) async{
    return StateHome(
      todayReminders: await _repository.today,
      tomorrowReminders: await _repository.tomorrow,
    );
  }

  Future<ReminderState> _mapAllToState(ReminderEvent _) async{
    return StateAll().fromMap(await _repository.getAll());
  }

  Future<ReminderState> _mapAddToState(EventAdd event) async{
    int id = await _repository.addRem(event.reminder);

    await NotificationManager().scheduleNotification(
        id: id,
        title: 'Task: ${event.reminder.title}',
        desc: (event.reminder.notes.isNotEmpty)
            ?'Notes:  ${event.reminder.notes}'
            :'Click to view',
        time: event.reminder.dateTime
    );

    add(EventSnackBar(
        message: 'Task ${event.reminder.title} has been created succesfully'));
    add(EventChangeMainFocus(dateTime: event.reminder.dateTime));
    return StateLoading();
  }

  Future<ReminderState> _mapEditToState(EventEdit event) async{
    await _repository.edit(event.reminder);

    NotificationManager().scheduleNotification(
        id: event.reminder.id,
        title: 'Task: ${event.reminder.title}',
        desc: (event.reminder.notes.isNotEmpty)
            ?'Notes:  ${event.reminder.notes}'
            :'Click to view',
        time: event.reminder.dateTime
    );

    add(EventSnackBar(
        message: 'Task ${event.reminder.title} has been edited succesfully'));
    return StateLoading();
  }

  Future<ReminderState> _mapCompletedToState(EventCompleted event) async{
    var title = await _repository.markComplete(event.id);

    NotificationManager().cancelNotification(event.id);

    if(title==null){
      add(EventSnackBar(message: 'There was an error, please try again!'));
      return StateLoading();
    }
    add(EventSnackBar(
        message: 'Task $title has been marked complete',
        withUndo: true,
      onUndo: () async{
          var reminder = await _repository.markInComplete(event.id);
          NotificationManager().scheduleNotification(
              id: reminder.id,
              title: 'Task: ${reminder.title}',
              desc: (reminder.notes.isNotEmpty)
                  ?'Notes:  ${reminder.notes}'
                  :'Click to view',
              time: reminder.dateTime
          );
          add(EventLoading());
      }
    ),);
    return StateLoading();
  }

  Future<ReminderState> _mapDeleteToState(EventDelete event) async{
    var reminder = await _repository.delete(event.id);
    if(reminder==null){
      add(EventSnackBar(message: 'There was an error, please try again!'));
      return StateLoading();
    }
    NotificationManager().cancelNotification(event.id);
    add(EventSnackBar(
        message: 'Task ${reminder.title} has been deleted succesfully',
        withUndo: true,
      onUndo: () async{
          int id = await _repository.addRem(reminder);
          add(EventLoading());
          NotificationManager().scheduleNotification(
              id: id,
              title: 'Task: ${reminder.title}',
              desc: (reminder.notes.isNotEmpty)
                  ?'Notes:  ${reminder.notes}'
                  :'Click to view',
              time: reminder.dateTime
          );
      },
    ));
    return StateLoading();
  }

  ReminderState _mapSnackBarToState(EventSnackBar event) {
    return StateShowSnackBar(
        message: event.message,
        withUndo: event.withUndo,
        onUndo: event.onUndo
    );
  }
  ///
  /// MainFocus And FineFocus :
  /// These have been added to change the focus whenever the user creates a new
  /// reminder, redirecting them to the created reminder, to add liveliness to app
  ///
  /// MainFocus: Handles changing to HomePage And AllPage TabView
  /// FineFocus: Handles changing tabs within HomePage and AllPage tabs
  /// Eg: In Home MainFocus - Today, Tomorrow is FineFocus
  /// Here main focus I have separated main focus and fine focus to be able to
  /// give time for HomeTab and AllTab to initialise it's children and then
  /// navigate to it, I tried without it, it was a problem
  ///

  ReminderState _mapFocusMainToState(EventChangeMainFocus event) {
    var focusByDate = _checkDate(event.dateTime);
    add(EventChangeFineFocus(fineFocus: focusByDate));
    if(focusByDate==focus.today || focusByDate==focus.tomorrow)
      return StateFocusMain(tabIndex: 0);
    else
      return StateFocusMain(tabIndex: 1);
  }

  Future<ReminderState> _mapFocusFineToState(EventChangeFineFocus event) async{
    await Future.delayed(Duration(milliseconds: 500));
    return StateFineFocus(tabIndex: event.fineFocus);
  }

  _checkDate(DateTime dateTime){
    final tDate = DateTime.now();
    final _date = DateTime(tDate.year,tDate.month,tDate.day);
    if(dateTime.isBefore(tDate))
      return focus.overdue;
    else if(dateTime.difference(_date).inDays == 0)
      return focus.today;
    else if(dateTime.difference(_date).inDays == 1)
      return focus.tomorrow;
    else
      return focus.upcoming;
  }
}

enum focus{
  today, tomorrow, upcoming, overdue
}