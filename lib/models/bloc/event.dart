import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/model.dart';

abstract class ReminderEvent extends Equatable {
  const ReminderEvent();

  @override
  List<Object> get props => [];
}

class EventAppStart extends ReminderEvent {
  const EventAppStart();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventAppStart';
}

class EventHome extends ReminderEvent {
  const EventHome();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventHome';
}

class EventAll extends ReminderEvent {
  const EventAll();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventAll';
}

class EventAdd extends ReminderEvent {
  final Reminder reminder;

  const EventAdd({
    @required this.reminder,
  });

  EventAdd copyWith({
    Reminder reminder,
  }) {
    return EventAdd(
      reminder: reminder ?? this.reminder,
    );
  }

  @override
  List<Object> get props => [reminder];

  @override
  String toString() => 'EventAdd {Reminder : ${reminder.title}';
}

class EventEdit extends ReminderEvent {
  final Reminder reminder;

  const EventEdit({
    @required this.reminder,
  });

  EventEdit copyWith({
    Reminder reminder,
  }) {
    return EventEdit(
      reminder: reminder ?? this.reminder,
    );
  }

  @override
  List<Object> get props => [reminder];

  @override
  String toString() => 'EventEdit {Reminder : ${reminder.title}';
}

class EventCompleted extends ReminderEvent {
  final int id;

  const EventCompleted({
    @required this.id,
  });

  EventCompleted copyWith({
    String id,
  }) {
    return EventCompleted(
      id: id ?? this.id,
    );
  }

  @override
  List<Object> get props => [id];

  @override
  String toString() => 'EventCompleted {Reminder : $id}';
}

class EventDelete extends ReminderEvent {
  final int id;

  const EventDelete({
    @required this.id,
  });

  EventDelete copyWith({
    String id,
  }) {
    return EventDelete(
      id: id ?? this.id,
    );
  }

  @override
  List<Object> get props => [id];

  @override
  String toString() => 'EventDelete {Reminder : $id}';
}

class EventSnackBar extends ReminderEvent {
  final String message;
  final bool withUndo;
  final Function onUndo;

  const EventSnackBar({
    @required this.message,
    this.withUndo = false,
    this.onUndo
  });

  @override
  List<Object> get props => [message, withUndo, onUndo];

  @override
  String toString() => 'EventSnackBar: Message : $message : Undo: $withUndo';
}

class EventChangeMainFocus extends ReminderEvent {
  final DateTime dateTime;

  const EventChangeMainFocus({
    this.dateTime,
  });

  @override
  List<Object> get props => [dateTime];

  @override
  String toString() => 'EventChangeMainFocus {TabIndex : $dateTime}';
}

class EventChangeFineFocus extends ReminderEvent {
  final focus fineFocus;

  const EventChangeFineFocus({this.fineFocus});

  @override
  List<Object> get props => [fineFocus];

  @override
  String toString() => 'EventChangeFineFocus : Focus: $fineFocus';
}

class EventLoading extends ReminderEvent{
  @override
  List<Object> get props => [];

  @override
  String toString() => 'EventLoading';
}