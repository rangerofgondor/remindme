import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:remindme/models/bloc/bloc.dart';
import 'package:remindme/models/model.dart';

abstract class ReminderState extends Equatable {
  const ReminderState();

  @override
  List<Object> get props => [];
}

class StateHome extends ReminderState {
  final List<Reminder> todayReminders;
  final List<Reminder> tomorrowReminders;

  const StateHome({
    this.todayReminders,
    this.tomorrowReminders,
  });

  StateHome copyWith({
    List<Reminder> todayReminders,
    List<Reminder> tomorrowReminders,
  }) {
    return StateHome(
      todayReminders: todayReminders ?? this.todayReminders,
      tomorrowReminders: tomorrowReminders ?? this.tomorrowReminders,
    );
  }

  @override
  List<Object> get props => [todayReminders, tomorrowReminders, /*customReminders*/];

  @override
  String toString() =>
      'StateHome {todayReminders: ${todayReminders.length},'
          ' tomorrowReminders: ${tomorrowReminders.length},'
          /*' customReminders: ${customReminders.length} }'*/;
}

class StateAll extends ReminderState {
  final List<GroupedReminders> upcomingReminders;
  final List<GroupedReminders> overdueReminders;
  final List<GroupedReminders> completedReminders;

  const StateAll({
    this.upcomingReminders,
    this.overdueReminders,
    this.completedReminders,
  });

  StateAll copyWith({
    GroupedReminders upcomingReminders,
    GroupedReminders overdueReminders,
    GroupedReminders completedReminders,
  }) {
    return StateAll(
      upcomingReminders: upcomingReminders ?? this.upcomingReminders,
      overdueReminders: overdueReminders ?? this.overdueReminders,
      completedReminders: completedReminders ?? this.completedReminders,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'upcoming': this.upcomingReminders,
      'overdue': this.overdueReminders,
      'completed': this.completedReminders,
    };
  }

  StateAll fromMap(dynamic obj) {
    return StateAll(
    upcomingReminders : obj['upcoming'],
    overdueReminders : obj['overdue'],
    completedReminders : obj['completed'],
    );
  }

  @override
  List<Object> get props => [upcomingReminders, overdueReminders, completedReminders];

  @override
  String toString() =>
      'StateAll {upcomingReminders: ${upcomingReminders.length},'
          ' overdueReminders: ${overdueReminders.length},'
          ' completedReminders: ${completedReminders.length} }';
}

class StateLoading extends ReminderState {
  const StateLoading();

  @override
  List<Object> get props => [];

  @override
  String toString() => 'StateLoading';
}

class StateShowSnackBar extends ReminderState {
  final String message;
  final bool withUndo;
  final Function onUndo;
  const StateShowSnackBar({@required this.message, this.withUndo = false, this.onUndo});

  @override
  List<Object> get props => [message, withUndo];

  @override
  String toString() => 'StateShowSnackBar : Message : $message Undo : $withUndo';
}

class StateFocusMain extends ReminderState {
  final int tabIndex;
  const StateFocusMain({@required this.tabIndex});

  @override
  List<Object> get props => [tabIndex];

  @override
  String toString() => 'StateFocusMain : Tab Index : $tabIndex';
}

class StateFineFocus extends ReminderState {
  final focus tabIndex;
  const StateFineFocus({@required this.tabIndex});

  @override
  List<Object> get props => [tabIndex];

  @override
  String toString() => 'StateFineFocus : Tab Index : $tabIndex';
}
