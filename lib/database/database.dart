import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

final remTbl = 'remindme';

class DatabaseProvider {
  static final DatabaseProvider databaseProvider = DatabaseProvider();

  Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    _database = await createDatabase();
    return _database;
  }

  createDatabase() async {
    Directory docDirectory = await getApplicationDocumentsDirectory();
    String path = join(docDirectory.path, "remindme.db");

    var database = await openDatabase(path,
        version: 1, onCreate: initDb, onUpgrade: onUpgrade);
    return database;
  }

  // Optional thing and I plan to use it later
  void onUpgrade(Database db, int oldVer, int newVer) {
    if (newVer > oldVer) {}
  }

  void initDb(Database db, int version) async {
    await db.execute(
        "CREATE TABLE $remTbl ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'title' TEXT, 'notes' TEXT, 'dateTime' TEXT, 'isCompleted' INTEGER);");
  }
}
