/*
  Database DAO ==> Data Access Object
  This file is dedicated to manage all local Databse Create Read Update Delete (CRUD),
  operations for our Reminder model asynchronously.
  This will be main communicator between repository and DatabaseProvider
*/

import 'dart:async';

import 'package:intl/intl.dart';
import 'package:remindme/database/database.dart';
import 'package:remindme/models/model.dart';

class RemDao {
  final dbProvider = DatabaseProvider.databaseProvider;

  // Adds new records
  Future<int> insertRem(Reminder reminder) async {
    final db = await dbProvider.database;
    var result = db.insert(
        remTbl, reminder.toMap()
    );
    if(await result > 0){
      var last = await db.query(
        remTbl,
        orderBy: 'id DESC',
        columns: ['id'],
        limit: 1
      );
      print('Last ID: ${last[0]['id']}');
      return last[0]['id'];
    }
    return -1;
  }

  // Get all records
  Future<List<Reminder>> getAllRems() async {
    final db = await dbProvider.database;
    var result = await db.rawQuery("SELECT * FROM $remTbl");
    return result.map((item) => Reminder().fromMap(item)).toList();
  }

  // Get records by date
  Future<List<Reminder>> getTodayRems() async {
    final db = await dbProvider.database;
    DateTime date = DateTime.now();
    List<Map<String, dynamic>> result;
    result = await db.rawQuery(
        ' SELECT * '
            'FROM $remTbl WHERE'
            ' dateTime LIKE "%${DateFormat('y-MM-dd').format(date)}%"'
    );
    return result.map((item) => Reminder().fromMap(item)).toList();
  }

  Future<List<Reminder>> getTomorrowRems() async {
    final db = await dbProvider.database;
    final tDate = DateTime.now();
    final date = DateTime(tDate.year,tDate.month,tDate.day+1);
    List<Map<String, dynamic>> result;
    result = await db.rawQuery(
        ' SELECT * '
            'FROM $remTbl WHERE'
            ' dateTime LIKE "%${DateFormat('y-MM-dd').format(date)}%"'
    );
    return result.map((item) => Reminder().fromMap(item)).toList();
  }

  // Update a record
  Future<int> updateRem(Reminder reminder) async {
    final db = await dbProvider.database;
    var result = await db.update(
      remTbl,
      reminder.toMap(),
      where: "id = ?",
      whereArgs: [reminder.id],
    );
    return result;
  }
  
  // Mark a record completed with specific id
  Future<Reminder> markCompletedById(int id, int complete) async {
    final db = await dbProvider.database;
    assert (complete == 0 || complete == 1);
    var result = await db.rawUpdate("UPDATE $remTbl SET isCompleted = $complete WHERE id = '$id'");
    if(result<=0){
      return null;
    }
    List<Map<String, dynamic>> reminder = await db.query(
      remTbl,
      where: 'id = ?',
      whereArgs: [id],
    );
    return Reminder().fromMap(reminder[0]);
  }

  // Delete a record
  Future<Reminder> deleteRem(int id) async {
    final db = await dbProvider.database;
    var reminderMap = await db.query(
      remTbl,
      where: 'id = ?',
      whereArgs: [id],
    );
    if(reminderMap.isEmpty){
      return null;
    }
    var reminder = Reminder().fromMap(reminderMap[0]);
    var result = await db.delete(
      remTbl,
      where: 'id = ?',
      whereArgs: [id],
    );
    if(result<=0){
      return null;
    }
    return reminder;
  }

  // Deleting whole table (I don't think we'll use this, but meh here it is)
  Future<int> deleteTbl() async {
    final db = await dbProvider.database;
    var result = await db.delete(remTbl);
    return result;
  }
}
